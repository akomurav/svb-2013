Procedure main

Decls

(declare-fun %4_0 () Real)
(declare-fun %tmp11_0 () Real)
(declare-fun %indvar_1_0 () Real)
(declare-fun %y.16_1_0 () Real)
(declare-fun bb.nph8_0 () Bool)
(declare-fun entry_0 () Bool)
(declare-fun E0x3e3e370 () Bool)
(declare-fun %6_0 () Bool)
(declare-fun %5_0 () Real)
(declare-fun %tmp11_1 () Real)
(declare-fun %4_1 () Real)
(declare-fun %indvar_1_1 () Real)
(declare-fun %y.16_1_1 () Real)
(declare-fun bb2.preheader_0 () Bool)
(declare-fun %8_0 () Bool)
(declare-fun %indvar.next_0 () Real)
(declare-fun %y.0.lcssa_0 () Real)
(declare-fun %tmp12_0 () Real)
(declare-fun %7_0 () Bool)
(declare-fun bb7.loopexit_0 () Bool)
(declare-fun E0x3e59ea0 () Bool)
(declare-fun %x.0.lcssa_0 () Real)
(declare-fun %9_0 () Bool)
(declare-fun E0x3e5aac0 () Bool)
(declare-fun %5_1 () Real)
(declare-fun __UFO__0_0 () Bool)
(declare-fun cp-rel-entry () Bool)
(declare-fun cp-rel-ERROR (Real Real ) Bool)
(declare-fun cp-rel-bb2.preheader (Real Real Real Real ) Bool)
(declare-fun cp-rel-__UFO__0 (Real Real ) Bool)


Rels

cp-rel-entry
cp-rel-ERROR
cp-rel-bb2.preheader
cp-rel-__UFO__0


Rules

cp-rel-entry <- true

(cp-rel-bb2.preheader |%4_0| |%tmp11_0| |%indvar_1_0| |%y.16_1_0| ) <- (and cp-rel-entry (let (($x30 (and bb.nph8_0 (and (<= |%indvar_1_0| 0.0) (>= |%indvar_1_0| 0.0)))))
(let (($x26 (and (=>  bb.nph8_0 (and (and entry_0 E0x3e3e370) |%6_0|)) (=>  bb.nph8_0 E0x3e3e370))))
(let (($x37 (and $x26 (and $x30 (and (<= |%y.16_1_0| |%5_0|) (>= |%y.16_1_0| |%5_0|))))))
(and $x37 (and (=  |%6_0| (> |%4_0| 0.0)) (= |%tmp11_0| (+ |%4_0| 1.0))))))))

(cp-rel-bb2.preheader |%4_1| |%tmp11_0| |%indvar_1_1| |%y.16_1_1| ) <- (and (cp-rel-bb2.preheader |%4_0| |%tmp11_1| |%indvar_1_0| |%y.16_1_0| ) (let (($x78 (= |%indvar.next_0| (+ |%indvar_1_0| 1.0))))
(let (($x76 (=  |%8_0| (> |%tmp12_0| 0.0))))
(let (($x74 (= |%y.0.lcssa_0| (ite  |%7_0| 0.0 |%y.16_1_0|))))
(let (($x72 (=  |%7_0| (< |%y.16_1_0| 0.0))))
(let (($x68 (= |%tmp12_0| (+ |%indvar_1_0| |%tmp11_0|))))
(let (($x57 (and (<= |%indvar_1_1| |%indvar.next_0|) (>= |%indvar_1_1| |%indvar.next_0|))))
(let (($x64 (and (and (and bb2.preheader_0 |%8_0|) $x57) (and (<= |%y.16_1_1| |%y.0.lcssa_0|) (>= |%y.16_1_1| |%y.0.lcssa_0|)))))
(and $x64 (and $x68 $x72 $x74 $x76 $x78))))))))))

(cp-rel-__UFO__0 |%4_0| |%5_0| ) <- (and cp-rel-entry (let (($x102 (=  |%9_0| (> |%x.0.lcssa_0| 0.0))))
(let (($x39 (=  |%6_0| (> |%4_0| 0.0))))
(let (($x103 (and $x39 $x102)))
(let (($x99 (and bb7.loopexit_0 (not |%9_0|))))
(let (($x92 (and (and (and entry_0 E0x3e59ea0) (not |%6_0|)) (and (<= |%x.0.lcssa_0| |%4_0|) (>= |%x.0.lcssa_0| |%4_0|)))))
(let (($x95 (and (=>  bb7.loopexit_0 $x92) (=>  bb7.loopexit_0 E0x3e59ea0))))
(and (and $x95 $x99) $x103))))))))

(cp-rel-__UFO__0 |%4_1| |%5_0| ) <- (and (cp-rel-bb2.preheader |%4_0| |%tmp11_1| |%indvar_1_0| |%y.16_1_0| ) (let (($x102 (=  |%9_0| (> |%x.0.lcssa_0| 0.0))))
(let (($x78 (= |%indvar.next_0| (+ |%indvar_1_0| 1.0))))
(let (($x76 (=  |%8_0| (> |%tmp12_0| 0.0))))
(let (($x74 (= |%y.0.lcssa_0| (ite  |%7_0| 0.0 |%y.16_1_0|))))
(let (($x72 (=  |%7_0| (< |%y.16_1_0| 0.0))))
(let (($x68 (= |%tmp12_0| (+ |%indvar_1_0| |%tmp11_0|))))
(let (($x117 (and $x68 $x72 $x74 $x76 $x78 $x102)))
(let (($x99 (and bb7.loopexit_0 (not |%9_0|))))
(let (($x112 (and (and (and bb2.preheader_0 E0x3e5aac0) (not |%8_0|)) (and (<= |%x.0.lcssa_0| |%tmp12_0|) (>= |%x.0.lcssa_0| |%tmp12_0|)))))
(let (($x115 (and (=>  bb7.loopexit_0 $x112) (=>  bb7.loopexit_0 E0x3e5aac0))))
(and (and $x115 $x99) $x117))))))))))))

(cp-rel-__UFO__0 |%4_1| |%5_1| ) <- (and (cp-rel-__UFO__0 |%4_0| |%5_0| ) __UFO__0_0)

(cp-rel-ERROR |%4_0| |%5_0| ) <- (and cp-rel-entry (let (($x102 (=  |%9_0| (> |%x.0.lcssa_0| 0.0))))
(let (($x39 (=  |%6_0| (> |%4_0| 0.0))))
(let (($x103 (and $x39 $x102)))
(let (($x122 (and bb7.loopexit_0 |%9_0|)))
(let (($x92 (and (and (and entry_0 E0x3e59ea0) (not |%6_0|)) (and (<= |%x.0.lcssa_0| |%4_0|) (>= |%x.0.lcssa_0| |%4_0|)))))
(let (($x95 (and (=>  bb7.loopexit_0 $x92) (=>  bb7.loopexit_0 E0x3e59ea0))))
(and (and $x95 $x122) $x103))))))))

(cp-rel-ERROR |%4_1| |%5_0| ) <- (and (cp-rel-bb2.preheader |%4_0| |%tmp11_1| |%indvar_1_0| |%y.16_1_0| ) (let (($x102 (=  |%9_0| (> |%x.0.lcssa_0| 0.0))))
(let (($x78 (= |%indvar.next_0| (+ |%indvar_1_0| 1.0))))
(let (($x76 (=  |%8_0| (> |%tmp12_0| 0.0))))
(let (($x74 (= |%y.0.lcssa_0| (ite  |%7_0| 0.0 |%y.16_1_0|))))
(let (($x72 (=  |%7_0| (< |%y.16_1_0| 0.0))))
(let (($x68 (= |%tmp12_0| (+ |%indvar_1_0| |%tmp11_0|))))
(let (($x117 (and $x68 $x72 $x74 $x76 $x78 $x102)))
(let (($x122 (and bb7.loopexit_0 |%9_0|)))
(let (($x112 (and (and (and bb2.preheader_0 E0x3e5aac0) (not |%8_0|)) (and (<= |%x.0.lcssa_0| |%tmp12_0|) (>= |%x.0.lcssa_0| |%tmp12_0|)))))
(let (($x115 (and (=>  bb7.loopexit_0 $x112) (=>  bb7.loopexit_0 E0x3e5aac0))))
(and (and $x115 $x122) $x117))))))))))))



Query

(cp-rel-ERROR |%4_0| |%5_0| )
