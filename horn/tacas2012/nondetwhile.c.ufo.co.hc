Procedure main

Decls

(declare-fun %5_1_0 () Real)
(declare-fun entry_0 () Bool)
(declare-fun %4_0 () Bool)
(declare-fun %3_0 () Real)
(declare-fun %5_1_1 () Real)
(declare-fun bb1_0 () Bool)
(declare-fun %7_0 () Bool)
(declare-fun %indvar.next_0 () Real)
(declare-fun %6_0 () Real)
(declare-fun bb4.loopexit_0 () Bool)
(declare-fun E0x3415a60 () Bool)
(declare-fun %x.0.lcssa_0 () Real)
(declare-fun %y.0.lcssa_0 () Real)
(declare-fun %9_0 () Bool)
(declare-fun %8_0 () Bool)
(declare-fun %tmp9_0 () Real)
(declare-fun bb4.loopexit.loopexit_0 () Bool)
(declare-fun E0x3413d20 () Bool)
(declare-fun E0x342f690 () Bool)
(declare-fun %tmp11_0 () Real)
(declare-fun __UFO__0_0 () Bool)
(declare-fun cp-rel-entry () Bool)
(declare-fun cp-rel-ERROR () Bool)
(declare-fun cp-rel-bb1 (Real ) Bool)
(declare-fun cp-rel-__UFO__0 () Bool)


Rels

cp-rel-entry
cp-rel-ERROR
cp-rel-bb1
cp-rel-__UFO__0


Rules

cp-rel-entry <- true

(cp-rel-bb1 |%5_1_0| ) <- (and cp-rel-entry (let (($x21 (=  |%4_0| (= |%3_0| 0.0))))
(let (($x17 (and (and entry_0 (not |%4_0|)) (and (<= |%5_1_0| 0.0) (>= |%5_1_0| 0.0)))))
(and $x17 $x21))))

(cp-rel-bb1 |%5_1_1| ) <- (and (cp-rel-bb1 |%5_1_0| ) (let (($x41 (= |%indvar.next_0| (+ |%5_1_0| 1.0))))
(let (($x39 (=  |%7_0| (= |%6_0| 0.0))))
(let (($x35 (and (and bb1_0 (not |%7_0|)) (and (<= |%5_1_1| |%indvar.next_0|) (>= |%5_1_1| |%indvar.next_0|)))))
(and $x35 (and $x39 $x41))))))

cp-rel-__UFO__0 <- (and cp-rel-entry (let (($x78 (=  |%9_0| (= |%y.0.lcssa_0| |%tmp9_0|))))
(let (($x76 (= |%tmp9_0| (ite  |%8_0| |%x.0.lcssa_0| 0.0))))
(let (($x72 (=  |%8_0| (> |%x.0.lcssa_0| 0.0))))
(let (($x21 (=  |%4_0| (= |%3_0| 0.0))))
(let (($x79 (and $x21 $x72 $x76 $x78)))
(let (($x67 (and bb4.loopexit_0 |%9_0|)))
(let (($x55 (and (and (and entry_0 E0x3415a60) |%4_0|) (and (<= |%x.0.lcssa_0| 10.0) (>= |%x.0.lcssa_0| 10.0)))))
(let (($x62 (=>  bb4.loopexit_0 (and $x55 (and (<= |%y.0.lcssa_0| 10.0) (>= |%y.0.lcssa_0| 10.0))))))
(let (($x64 (and $x62 (=>  bb4.loopexit_0 E0x3415a60))))
(and (and $x64 $x67) $x79)))))))))))

cp-rel-__UFO__0 <- (and (cp-rel-bb1 |%5_1_0| ) (let (($x78 (=  |%9_0| (= |%y.0.lcssa_0| |%tmp9_0|))))
(let (($x76 (= |%tmp9_0| (ite  |%8_0| |%x.0.lcssa_0| 0.0))))
(let (($x72 (=  |%8_0| (> |%x.0.lcssa_0| 0.0))))
(let (($x41 (= |%indvar.next_0| (+ |%5_1_0| 1.0))))
(let (($x39 (=  |%7_0| (= |%6_0| 0.0))))
(let (($x108 (and $x39 $x41 (= |%tmp11_0| (+ |%5_1_0| 11.0)) $x72 $x76 $x78)))
(let (($x67 (and bb4.loopexit_0 |%9_0|)))
(let (($x96 (and (and bb4.loopexit.loopexit_0 E0x342f690) (and (<= |%x.0.lcssa_0| |%tmp11_0|) (>= |%x.0.lcssa_0| |%tmp11_0|)))))
(let (($x100 (and $x96 (and (<= |%y.0.lcssa_0| |%tmp11_0|) (>= |%y.0.lcssa_0| |%tmp11_0|)))))
(let (($x103 (and (=>  bb4.loopexit_0 $x100) (=>  bb4.loopexit_0 E0x342f690))))
(let (($x86 (=>  bb4.loopexit.loopexit_0 (and (and bb1_0 E0x3413d20) |%7_0|))))
(let (($x88 (and $x86 (=>  bb4.loopexit.loopexit_0 E0x3413d20))))
(and (and $x88 $x103 $x67) $x108))))))))))))))

cp-rel-__UFO__0 <- (and cp-rel-__UFO__0 __UFO__0_0)

cp-rel-ERROR <- (and cp-rel-entry (let (($x78 (=  |%9_0| (= |%y.0.lcssa_0| |%tmp9_0|))))
(let (($x76 (= |%tmp9_0| (ite  |%8_0| |%x.0.lcssa_0| 0.0))))
(let (($x72 (=  |%8_0| (> |%x.0.lcssa_0| 0.0))))
(let (($x21 (=  |%4_0| (= |%3_0| 0.0))))
(let (($x79 (and $x21 $x72 $x76 $x78)))
(let (($x113 (and bb4.loopexit_0 (not |%9_0|))))
(let (($x55 (and (and (and entry_0 E0x3415a60) |%4_0|) (and (<= |%x.0.lcssa_0| 10.0) (>= |%x.0.lcssa_0| 10.0)))))
(let (($x62 (=>  bb4.loopexit_0 (and $x55 (and (<= |%y.0.lcssa_0| 10.0) (>= |%y.0.lcssa_0| 10.0))))))
(let (($x64 (and $x62 (=>  bb4.loopexit_0 E0x3415a60))))
(and (and $x64 $x113) $x79)))))))))))

cp-rel-ERROR <- (and (cp-rel-bb1 |%5_1_0| ) (let (($x78 (=  |%9_0| (= |%y.0.lcssa_0| |%tmp9_0|))))
(let (($x76 (= |%tmp9_0| (ite  |%8_0| |%x.0.lcssa_0| 0.0))))
(let (($x72 (=  |%8_0| (> |%x.0.lcssa_0| 0.0))))
(let (($x41 (= |%indvar.next_0| (+ |%5_1_0| 1.0))))
(let (($x39 (=  |%7_0| (= |%6_0| 0.0))))
(let (($x108 (and $x39 $x41 (= |%tmp11_0| (+ |%5_1_0| 11.0)) $x72 $x76 $x78)))
(let (($x113 (and bb4.loopexit_0 (not |%9_0|))))
(let (($x96 (and (and bb4.loopexit.loopexit_0 E0x342f690) (and (<= |%x.0.lcssa_0| |%tmp11_0|) (>= |%x.0.lcssa_0| |%tmp11_0|)))))
(let (($x100 (and $x96 (and (<= |%y.0.lcssa_0| |%tmp11_0|) (>= |%y.0.lcssa_0| |%tmp11_0|)))))
(let (($x103 (and (=>  bb4.loopexit_0 $x100) (=>  bb4.loopexit_0 E0x342f690))))
(let (($x86 (=>  bb4.loopexit.loopexit_0 (and (and bb1_0 E0x3413d20) |%7_0|))))
(let (($x88 (and $x86 (=>  bb4.loopexit.loopexit_0 E0x3413d20))))
(and (and $x88 $x103 $x113) $x108))))))))))))))



Query

cp-rel-ERROR
