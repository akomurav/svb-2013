; ModuleID = './tests/bench2/ntdrivers-simplified/floppy_simpl4_BUG.cil.co.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

define i32 @main() nounwind uwtable {
if.then:
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !70), !dbg !72
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !74), !dbg !75
  tail call void @llvm.dbg.value(metadata !77, i64 0, metadata !74), !dbg !78
  %call = tail call i32 (...)* @nondet_int() nounwind, !dbg !80
  tail call void @llvm.dbg.value(metadata !{i32 %call}, i64 0, metadata !81), !dbg !80
  %cmp6 = icmp eq i32 %call, 0, !dbg !82
  br i1 %cmp6, label %return, label %if.else, !dbg !82

if.else:                                          ; preds = %if.then
  %call8 = tail call i32 (...)* @nondet_int() nounwind, !dbg !83
  tail call void @llvm.dbg.value(metadata !{i32 %call8}, i64 0, metadata !85), !dbg !83
  %cmp9 = icmp eq i32 %call8, 1, !dbg !86
  br i1 %cmp9, label %return, label %if.else11, !dbg !86

if.else11:                                        ; preds = %if.else
  %call12 = tail call i32 (...)* @nondet_int() nounwind, !dbg !87
  tail call void @llvm.dbg.value(metadata !{i32 %call12}, i64 0, metadata !89), !dbg !87
  %cmp13 = icmp eq i32 %call12, 2, !dbg !90
  br i1 %cmp13, label %switch_2_2, label %if.else15, !dbg !90

if.else15:                                        ; preds = %if.else11
  %call16 = tail call i32 (...)* @nondet_int() nounwind, !dbg !91
  tail call void @llvm.dbg.value(metadata !{i32 %call16}, i64 0, metadata !93), !dbg !91
  %cmp17 = icmp eq i32 %call16, 3, !dbg !94
  br i1 %cmp17, label %switch_2_3, label %return, !dbg !94

switch_2_2:                                       ; preds = %if.else11
  %0 = tail call i32 @nondet_1() nounwind
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !95), !dbg !97
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !101), !dbg !102
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !103), !dbg !104
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !105), !dbg !106
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !107), !dbg !108
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !109), !dbg !110
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !111), !dbg !112
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !113), !dbg !114
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !115), !dbg !116
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !117), !dbg !118
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !119), !dbg !120
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !121), !dbg !122
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !123), !dbg !124
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !125), !dbg !126
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !127), !dbg !128
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !129), !dbg !130
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !131), !dbg !132
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !133), !dbg !134
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !135), !dbg !136
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !137), !dbg !138
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !139), !dbg !140
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !141), !dbg !142
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !143), !dbg !144
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !145), !dbg !146
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !147), !dbg !148
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !149), !dbg !150
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !151), !dbg !152
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !153), !dbg !154
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !155), !dbg !156
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !157), !dbg !158
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !159), !dbg !160
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !161), !dbg !162
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !163), !dbg !164
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !165), !dbg !166
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !167), !dbg !168
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !169), !dbg !170
  %tobool.i = icmp eq i32 %0, 0, !dbg !171
  br i1 %tobool.i, label %if.end3.i.i, label %return, !dbg !171

if.end3.i.i:                                      ; preds = %switch_2_2
  %call4.i.i = tail call i32 (...)* @nondet_int() nounwind, !dbg !173
  tail call void @llvm.dbg.value(metadata !{i32 %call4.i.i}, i64 0, metadata !178), !dbg !173
  %cmp5.i.i = icmp eq i32 %call4.i.i, 0, !dbg !179
  br i1 %cmp5.i.i, label %return, label %if.else.i14.i, !dbg !179

if.else.i14.i:                                    ; preds = %if.end3.i.i
  %call8.i.i = tail call i32 (...)* @nondet_int() nounwind, !dbg !180
  tail call void @llvm.dbg.value(metadata !{i32 %call8.i.i}, i64 0, metadata !182), !dbg !180
  br label %return, !dbg !183

switch_2_3:                                       ; preds = %if.else15
  %1 = tail call i32 @nondet_1() nounwind
  tail call void @llvm.dbg.value(metadata !{i32 %1}, i64 0, metadata !184), !dbg !186
  tail call void @llvm.dbg.value(metadata !{i32 %1}, i64 0, metadata !188), !dbg !189
  tail call void @llvm.dbg.value(metadata !{i32 %1}, i64 0, metadata !190), !dbg !191
  tail call void @llvm.dbg.value(metadata !{i32 %1}, i64 0, metadata !192), !dbg !193
  tail call void @llvm.dbg.value(metadata !{i32 %1}, i64 0, metadata !194), !dbg !195
  tail call void @llvm.dbg.value(metadata !{i32 %1}, i64 0, metadata !196), !dbg !197
  tail call void @llvm.dbg.value(metadata !{i32 %1}, i64 0, metadata !198), !dbg !199
  tail call void @llvm.dbg.value(metadata !{i32 %1}, i64 0, metadata !200), !dbg !201
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !202), !dbg !203
  tail call void @llvm.dbg.value(metadata !{i32 %1}, i64 0, metadata !205), !dbg !206
  %tobool.i83 = icmp ne i32 %1, 0, !dbg !207
  br i1 %tobool.i83, label %return, label %if.end3.i.i.i, !dbg !207

if.end3.i.i.i:                                    ; preds = %switch_2_3
  tail call void @llvm.dbg.value(metadata !{i32 %1}, i64 0, metadata !208), !dbg !210
  tail call void @llvm.dbg.value(metadata !{i32 %1}, i64 0, metadata !214), !dbg !215
  tail call void @llvm.dbg.value(metadata !{i32 %1}, i64 0, metadata !216), !dbg !217
  tail call void @llvm.dbg.value(metadata !{i32 %1}, i64 0, metadata !218), !dbg !219
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !220), !dbg !221
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !223), !dbg !224
  tail call void @llvm.dbg.value(metadata !{i32 %1}, i64 0, metadata !225), !dbg !226
  tail call void @llvm.dbg.value(metadata !{i32 %1}, i64 0, metadata !227), !dbg !228
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !229), !dbg !230
  tail call void @llvm.dbg.value(metadata !231, i64 0, metadata !232), !dbg !233
  tail call void @llvm.dbg.value(metadata !234, i64 0, metadata !235), !dbg !236
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !243), !dbg !244
  tail call void @llvm.dbg.value(metadata !245, i64 0, metadata !246), !dbg !240
  tail call void @llvm.dbg.value(metadata !247, i64 0, metadata !248), !dbg !250
  %call4.i.i.i = tail call i32 (...)* @nondet_int() nounwind, !dbg !251
  tail call void @llvm.dbg.value(metadata !{i32 %call4.i.i.i}, i64 0, metadata !178), !dbg !251
  %cmp5.i.i.i = icmp eq i32 %call4.i.i.i, 0, !dbg !252
  br i1 %cmp5.i.i.i, label %if.end10.i.i, label %if.else.i.i.i98, !dbg !252

if.else.i.i.i98:                                  ; preds = %if.end3.i.i.i
  %call8.i.i.i = tail call i32 (...)* @nondet_int() nounwind, !dbg !253
  tail call void @llvm.dbg.value(metadata !{i32 %call8.i.i.i}, i64 0, metadata !182), !dbg !253
  %cmp9.i.i.i = icmp eq i32 %call8.i.i.i, 1, !dbg !254
  br i1 %cmp9.i.i.i, label %if.end10.i.i, label %if.then8.i.i, !dbg !254

if.then8.i.i:                                     ; preds = %if.else.i.i.i98
  tail call void @llvm.dbg.value(metadata !255, i64 0, metadata !256), !dbg !242
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !257), !dbg !258
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !261), !dbg !262
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !263), !dbg !264
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !265), !dbg !266
  %call.i.i.i100 = tail call i32 (...)* @nondet_int() nounwind, !dbg !267
  tail call void @llvm.dbg.value(metadata !{i32 %call.i.i.i100}, i64 0, metadata !270), !dbg !267
  br label %if.end10.i.i, !dbg !271

if.end10.i.i:                                     ; preds = %if.then8.i.i, %if.else.i.i.i98, %if.end3.i.i.i
  %2 = phi i32 [ 259, %if.then8.i.i ], [ 0, %if.end3.i.i.i ], [ -1073741823, %if.else.i.i.i98 ]
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !272), !dbg !273
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !274), !dbg !275
  tail call void @llvm.dbg.value(metadata !{i32 %1}, i64 0, metadata !276), !dbg !278
  tail call void @llvm.dbg.value(metadata !{i32 %1}, i64 0, metadata !280), !dbg !282
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !285), !dbg !286
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !287), !dbg !288
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !289), !dbg !290
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !291), !dbg !292
  tail call void @llvm.dbg.value(metadata !234, i64 0, metadata !293), !dbg !294
  %call.i.i.i.i = tail call i32 (...)* @nondet_int() nounwind, !dbg !295
  tail call void @llvm.dbg.value(metadata !{i32 %call.i.i.i.i}, i64 0, metadata !297), !dbg !295
  tail call void @llvm.dbg.value(metadata !{i32 %1}, i64 0, metadata !298), !dbg !283
  br i1 true, label %if.end47.i.i, label %if.then2.i.i.i.i, !dbg !299

if.then2.i.i.i.i:                                 ; preds = %if.end10.i.i
  br i1 true, label %if.end3.i.i.i.i, label %if.else.i.i.i.i.i, !dbg !300

if.else.i.i.i.i.i:                                ; preds = %if.then2.i.i.i.i
  br label %if.end3.i.i.i.i

if.end3.i.i.i.i:                                  ; preds = %if.else.i.i.i.i.i, %if.then2.i.i.i.i
  br i1 undef, label %if.end17.i.i.i.i, label %if.else.i.i13.i.i, !dbg !306

if.else.i.i13.i.i:                                ; preds = %if.end3.i.i.i.i
  br i1 undef, label %if.end17.i.i.i.i, label %switch_8_default.i.i.i.i, !dbg !307

switch_8_default.i.i.i.i:                         ; preds = %if.else.i.i13.i.i
  br label %if.end17.i.i.i.i, !dbg !308

if.end17.i.i.i.i:                                 ; preds = %switch_8_default.i.i.i.i, %if.else.i.i13.i.i, %if.end3.i.i.i.i
  br label %NodeBlock

NodeBlock:                                        ; preds = %if.end17.i.i.i.i
  %Pivot = icmp slt i32 undef, 5
  br i1 %Pivot, label %LeafBlock, label %LeafBlock198

LeafBlock198:                                     ; preds = %NodeBlock
  %SwitchLeaf199 = icmp eq i32 undef, 5
  br i1 %SwitchLeaf199, label %if.then24.i.i.i.i, label %NewDefault

LeafBlock:                                        ; preds = %NodeBlock
  %SwitchLeaf = icmp eq i32 undef, 1
  br i1 %SwitchLeaf, label %IofCallDriver.exit.i.i.i, label %NewDefault

if.then24.i.i.i.i:                                ; preds = %LeafBlock198
  br i1 undef, label %if.end11.i.i.i.i, label %FlFdcDeviceIo.exit.i.i, !dbg !311

NewDefault:                                       ; preds = %LeafBlock, %LeafBlock198
  br label %if.else30.i.i.i.i

if.else30.i.i.i.i:                                ; preds = %NewDefault
  br i1 undef, label %IofCallDriver.exit.i.i.i, label %if.else34.i.i.i.i, !dbg !313

if.else34.i.i.i.i:                                ; preds = %if.else30.i.i.i.i
  br label %IofCallDriver.exit.i.i.i

IofCallDriver.exit.i.i.i:                         ; preds = %if.else34.i.i.i.i, %if.else30.i.i.i.i, %LeafBlock
  br i1 undef, label %if.end11.i.i.i.i, label %FlFdcDeviceIo.exit.i.i, !dbg !314

if.end11.i.i.i.i:                                 ; preds = %IofCallDriver.exit.i.i.i, %if.then24.i.i.i.i
  br label %FlFdcDeviceIo.exit.i.i, !dbg !315

FlFdcDeviceIo.exit.i.i:                           ; preds = %if.end11.i.i.i.i, %IofCallDriver.exit.i.i.i, %if.then24.i.i.i.i
  br i1 undef, label %if.then14.i.i, label %if.end47.i.i, !dbg !318

if.then14.i.i:                                    ; preds = %FlFdcDeviceIo.exit.i.i
  br i1 %tobool.i83, label %if.end39.i.i, label %if.end31.i.i, !dbg !319

if.end31.i.i:                                     ; preds = %if.then14.i.i
  br i1 undef, label %if.end39.i.i, label %if.end47.i.i, !dbg !321

if.end39.i.i:                                     ; preds = %if.end31.i.i, %if.then14.i.i
  br i1 undef, label %if.then43.i.i, label %if.end47.i.i, !dbg !322

if.then43.i.i:                                    ; preds = %if.end39.i.i
  br label %if.end47.i.i, !dbg !327

if.end47.i.i:                                     ; preds = %if.then43.i.i, %if.end39.i.i, %if.end31.i.i, %FlFdcDeviceIo.exit.i.i, %if.end10.i.i
  %3 = phi i32 [ undef, %if.end31.i.i ], [ undef, %FlFdcDeviceIo.exit.i.i ], [ undef, %if.then43.i.i ], [ undef, %if.end39.i.i ], [ %2, %if.end10.i.i ]
  tail call void @llvm.dbg.value(metadata !332, i64 0, metadata !333), !dbg !334
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !335), !dbg !336
  br i1 true, label %if.then47, label %_L___1, !dbg !338

_L___1:                                           ; preds = %if.end47.i.i
  br i1 false, label %return, label %if.then47, !dbg !341

if.then47:                                        ; preds = %_L___1, %if.end47.i.i
  %.ph168175 = phi i32 [ 1, %_L___1 ], [ 2, %if.end47.i.i ]
  br i1 false, label %return, label %if.then49, !dbg !342

if.then49:                                        ; preds = %if.then47
  br label %NodeBlock209

NodeBlock209:                                     ; preds = %if.then49
  %Pivot210 = icmp slt i32 %.ph168175, 4
  br i1 %Pivot210, label %LeafBlock201, label %NodeBlock207

NodeBlock207:                                     ; preds = %NodeBlock209
  %Pivot208 = icmp slt i32 %.ph168175, 7
  br i1 %Pivot208, label %LeafBlock203, label %LeafBlock205

LeafBlock205:                                     ; preds = %NodeBlock207
  %SwitchLeaf206 = icmp eq i32 %.ph168175, 7
  br i1 %SwitchLeaf206, label %if.else72, label %NewDefault200

LeafBlock203:                                     ; preds = %NodeBlock207
  %SwitchLeaf204 = icmp eq i32 %.ph168175, 4
  br i1 %SwitchLeaf204, label %if.else72, label %NewDefault200

LeafBlock201:                                     ; preds = %NodeBlock209
  %SwitchLeaf202 = icmp eq i32 %.ph168175, 2
  br i1 %SwitchLeaf202, label %if.then68, label %NewDefault200

NewDefault200:                                    ; preds = %LeafBlock201, %LeafBlock203, %LeafBlock205
  br label %if.end59

if.end59:                                         ; preds = %NewDefault200
  br label %UnifiedReturnBlock

if.then68:                                        ; preds = %LeafBlock201
  br i1 false, label %if.then70, label %return, !dbg !344

if.then70:                                        ; preds = %if.then68
  br label %return, !dbg !347

if.else72:                                        ; preds = %LeafBlock203, %LeafBlock205
  %cmp73 = icmp eq i32 %3, -1073741670, !dbg !349
  br i1 %cmp73, label %return, label %if.then74, !dbg !349

if.then74:                                        ; preds = %if.else72
  br label %UnifiedReturnBlock

return:                                           ; preds = %if.else72, %if.then70, %if.then68, %if.then47, %_L___1, %switch_2_3, %if.else.i14.i, %if.end3.i.i, %switch_2_2, %if.else15, %if.else, %if.then
  br label %_UFO__exit, !dbg !350

_UFO__exit:                                       ; preds = %_UFO__exit, %return
  br label %_UFO__exit

UnifiedReturnBlock:                               ; preds = %if.then74, %if.end59
  %UnifiedRetVal = phi i32 [ 5, %if.end59 ], [ 5, %if.then74 ]
  ret i32 %UnifiedRetVal
}

declare i32 @nondet_int(...)

declare i32 @nondet_1() readnone

declare i64 @nondet_2() readnone

declare i32 @exit(i32)

declare void @llvm.lifetime.start(i64, i8* nocapture) nounwind

declare void @llvm.lifetime.end(i64, i8* nocapture) nounwind

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

!llvm.dbg.cu = !{!0}

!0 = metadata !{i32 720913, i32 0, i32 12, metadata !"./tests/bench2/ntdrivers-simplified/floppy_simpl4_BUG.cil.c.CIL.c", metadata !"/ag/svn/interp/trunk/srcufo/src/lib/LBE", metadata !"clang version 3.0 (branches/release_30 146682)", i1 true, i1 false, metadata !"", i32 0, metadata !1, metadata !1, metadata !3, metadata !42} ; [ DW_TAG_compile_unit ]
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !5, metadata !11, metadata !16, metadata !17, metadata !18, metadata !19, metadata !20, metadata !21, metadata !22, metadata !23, metadata !24, metadata !25, metadata !26, metadata !27, metadata !28, metadata !29, metadata !30, metadata !31, metadata !32, metadata !33, metadata !34, metadata !35, metadata !36, metadata !37, metadata !38, metadata !39, metadata !40, metadata !41}
!5 = metadata !{i32 720942, i32 0, metadata !6, metadata !"_BLAST_init", metadata !"_BLAST_init", metadata !"", metadata !6, i32 27, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!6 = metadata !{i32 720937, metadata !"floppy_simpl4_BUG.cil.c", metadata !"/ag/svn/interp/trunk/srcufo/src/lib/LBE", null} ; [ DW_TAG_file_type ]
!7 = metadata !{i32 720917, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i32 0, i32 0, i32 0, metadata !8, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!8 = metadata !{null}
!9 = metadata !{metadata !10}
!10 = metadata !{i32 720932}                      ; [ DW_TAG_base_type ]
!11 = metadata !{i32 720942, i32 0, metadata !12, metadata !"FlAcpiConfigureFloppy", metadata !"FlAcpiConfigureFloppy", metadata !"", metadata !12, i32 95, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!12 = metadata !{i32 720937, metadata !"floppy_simpl4.cil.c", metadata !"/ag/svn/interp/trunk/srcufo/src/lib/LBE", null} ; [ DW_TAG_file_type ]
!13 = metadata !{i32 720917, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i32 0, i32 0, i32 0, metadata !14, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!14 = metadata !{metadata !15}
!15 = metadata !{i32 720932, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!16 = metadata !{i32 720942, i32 0, metadata !12, metadata !"FlQueueIrpToThread", metadata !"FlQueueIrpToThread", metadata !"", metadata !12, i32 102, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!17 = metadata !{i32 720942, i32 0, metadata !12, metadata !"FloppyPnp", metadata !"FloppyPnp", metadata !"", metadata !12, i32 171, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!18 = metadata !{i32 720942, i32 0, metadata !12, metadata !"FloppyStartDevice", metadata !"FloppyStartDevice", metadata !"", metadata !12, i32 464, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!19 = metadata !{i32 720942, i32 0, metadata !12, metadata !"FloppyPnpComplete", metadata !"FloppyPnpComplete", metadata !"", metadata !12, i32 632, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!20 = metadata !{i32 720942, i32 0, metadata !12, metadata !"FlFdcDeviceIo", metadata !"FlFdcDeviceIo", metadata !"", metadata !12, i32 642, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!21 = metadata !{i32 720942, i32 0, metadata !12, metadata !"FloppyProcessQueuedRequests", metadata !"FloppyProcessQueuedRequests", metadata !"", metadata !12, i32 677, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!22 = metadata !{i32 720942, i32 0, metadata !12, metadata !"stub_driver_init", metadata !"stub_driver_init", metadata !"", metadata !12, i32 684, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!23 = metadata !{i32 720942, i32 0, metadata !12, metadata !"main", metadata !"main", metadata !"", metadata !12, i32 697, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, i32 ()* @main, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!24 = metadata !{i32 720942, i32 0, metadata !12, metadata !"IoBuildDeviceIoControlRequest", metadata !"IoBuildDeviceIoControlRequest", metadata !"", metadata !12, i32 855, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!25 = metadata !{i32 720942, i32 0, metadata !12, metadata !"IoDeleteSymbolicLink", metadata !"IoDeleteSymbolicLink", metadata !"", metadata !12, i32 878, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!26 = metadata !{i32 720942, i32 0, metadata !12, metadata !"IoQueryDeviceDescription", metadata !"IoQueryDeviceDescription", metadata !"", metadata !12, i32 901, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!27 = metadata !{i32 720942, i32 0, metadata !12, metadata !"IoRegisterDeviceInterface", metadata !"IoRegisterDeviceInterface", metadata !"", metadata !12, i32 923, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!28 = metadata !{i32 720942, i32 0, metadata !12, metadata !"IoSetDeviceInterfaceState", metadata !"IoSetDeviceInterfaceState", metadata !"", metadata !12, i32 944, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!29 = metadata !{i32 720942, i32 0, metadata !12, metadata !"stubMoreProcessingRequired", metadata !"stubMoreProcessingRequired", metadata !"", metadata !12, i32 965, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!30 = metadata !{i32 720942, i32 0, metadata !12, metadata !"IofCallDriver", metadata !"IofCallDriver", metadata !"", metadata !12, i32 979, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!31 = metadata !{i32 720942, i32 0, metadata !12, metadata !"IofCompleteRequest", metadata !"IofCompleteRequest", metadata !"", metadata !12, i32 1050, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!32 = metadata !{i32 720942, i32 0, metadata !12, metadata !"KeSetEvent", metadata !"KeSetEvent", metadata !"", metadata !12, i32 1064, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!33 = metadata !{i32 720942, i32 0, metadata !12, metadata !"KeWaitForSingleObject", metadata !"KeWaitForSingleObject", metadata !"", metadata !12, i32 1073, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!34 = metadata !{i32 720942, i32 0, metadata !12, metadata !"ObReferenceObjectByHandle", metadata !"ObReferenceObjectByHandle", metadata !"", metadata !12, i32 1117, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!35 = metadata !{i32 720942, i32 0, metadata !12, metadata !"PsCreateSystemThread", metadata !"PsCreateSystemThread", metadata !"", metadata !12, i32 1139, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!36 = metadata !{i32 720942, i32 0, metadata !12, metadata !"ZwClose", metadata !"ZwClose", metadata !"", metadata !12, i32 1160, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!37 = metadata !{i32 720942, i32 0, metadata !12, metadata !"FloppyCreateClose", metadata !"FloppyCreateClose", metadata !"", metadata !12, i32 1181, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!38 = metadata !{i32 720942, i32 0, metadata !12, metadata !"FloppyDeviceControl", metadata !"FloppyDeviceControl", metadata !"", metadata !12, i32 1196, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!39 = metadata !{i32 720942, i32 0, metadata !12, metadata !"FlCheckFormatParameters", metadata !"FlCheckFormatParameters", metadata !"", metadata !12, i32 1470, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!40 = metadata !{i32 720942, i32 0, metadata !12, metadata !"FloppyQueueRequest", metadata !"FloppyQueueRequest", metadata !"", metadata !12, i32 1487, metadata !13, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!41 = metadata !{i32 720942, i32 0, metadata !12, metadata !"errorFn", metadata !"errorFn", metadata !"", metadata !12, i32 1532, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!42 = metadata !{metadata !43}
!43 = metadata !{metadata !44, metadata !45, metadata !46, metadata !47, metadata !48, metadata !49, metadata !50, metadata !51, metadata !52, metadata !53, metadata !54, metadata !55, metadata !56, metadata !57, metadata !58, metadata !59, metadata !60, metadata !61, metadata !62, metadata !63, metadata !64, metadata !65, metadata !66, metadata !67, metadata !68, metadata !69}
!44 = metadata !{i32 720948, i32 0, null, metadata !"PagingReferenceCount", metadata !"PagingReferenceCount", metadata !"", metadata !12, i32 92, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!45 = metadata !{i32 720948, i32 0, null, metadata !"PagingMutex", metadata !"PagingMutex", metadata !"", metadata !12, i32 93, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!46 = metadata !{i32 720948, i32 0, null, metadata !"FloppyThread", metadata !"FloppyThread", metadata !"", metadata !6, i32 1, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!47 = metadata !{i32 720948, i32 0, null, metadata !"KernelMode", metadata !"KernelMode", metadata !"", metadata !6, i32 2, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!48 = metadata !{i32 720948, i32 0, null, metadata !"Suspended", metadata !"Suspended", metadata !"", metadata !6, i32 3, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!49 = metadata !{i32 720948, i32 0, null, metadata !"Executive", metadata !"Executive", metadata !"", metadata !6, i32 4, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!50 = metadata !{i32 720948, i32 0, null, metadata !"DiskController", metadata !"DiskController", metadata !"", metadata !6, i32 5, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!51 = metadata !{i32 720948, i32 0, null, metadata !"FloppyDiskPeripheral", metadata !"FloppyDiskPeripheral", metadata !"", metadata !6, i32 6, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!52 = metadata !{i32 720948, i32 0, null, metadata !"FlConfigCallBack", metadata !"FlConfigCallBack", metadata !"", metadata !6, i32 7, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!53 = metadata !{i32 720948, i32 0, null, metadata !"MaximumInterfaceType", metadata !"MaximumInterfaceType", metadata !"", metadata !6, i32 8, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!54 = metadata !{i32 720948, i32 0, null, metadata !"MOUNTDEV_MOUNTED_DEVICE_GUID", metadata !"MOUNTDEV_MOUNTED_DEVICE_GUID", metadata !"", metadata !6, i32 9, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!55 = metadata !{i32 720948, i32 0, null, metadata !"myStatus", metadata !"myStatus", metadata !"", metadata !6, i32 10, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!56 = metadata !{i32 720948, i32 0, null, metadata !"s", metadata !"s", metadata !"", metadata !6, i32 11, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!57 = metadata !{i32 720948, i32 0, null, metadata !"UNLOADED", metadata !"UNLOADED", metadata !"", metadata !6, i32 12, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!58 = metadata !{i32 720948, i32 0, null, metadata !"NP", metadata !"NP", metadata !"", metadata !6, i32 13, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!59 = metadata !{i32 720948, i32 0, null, metadata !"DC", metadata !"DC", metadata !"", metadata !6, i32 14, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!60 = metadata !{i32 720948, i32 0, null, metadata !"SKIP1", metadata !"SKIP1", metadata !"", metadata !6, i32 15, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!61 = metadata !{i32 720948, i32 0, null, metadata !"SKIP2", metadata !"SKIP2", metadata !"", metadata !6, i32 16, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!62 = metadata !{i32 720948, i32 0, null, metadata !"MPR1", metadata !"MPR1", metadata !"", metadata !6, i32 17, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!63 = metadata !{i32 720948, i32 0, null, metadata !"MPR3", metadata !"MPR3", metadata !"", metadata !6, i32 18, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!64 = metadata !{i32 720948, i32 0, null, metadata !"IPC", metadata !"IPC", metadata !"", metadata !6, i32 19, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!65 = metadata !{i32 720948, i32 0, null, metadata !"pended", metadata !"pended", metadata !"", metadata !6, i32 20, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!66 = metadata !{i32 720948, i32 0, null, metadata !"compRegistered", metadata !"compRegistered", metadata !"", metadata !6, i32 21, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!67 = metadata !{i32 720948, i32 0, null, metadata !"lowerDriverReturn", metadata !"lowerDriverReturn", metadata !"", metadata !6, i32 22, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!68 = metadata !{i32 720948, i32 0, null, metadata !"setEventCalled", metadata !"setEventCalled", metadata !"", metadata !6, i32 23, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!69 = metadata !{i32 720948, i32 0, null, metadata !"customIrp", metadata !"customIrp", metadata !"", metadata !6, i32 24, metadata !15, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!70 = metadata !{i32 721152, metadata !71, metadata !"status", metadata !12, i32 697, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!71 = metadata !{i32 720907, metadata !23, i32 697, i32 1, metadata !12, i32 76} ; [ DW_TAG_lexical_block ]
!72 = metadata !{i32 707, i32 3, metadata !73, null}
!73 = metadata !{i32 720907, metadata !71, i32 708, i32 3, metadata !12, i32 77} ; [ DW_TAG_lexical_block ]
!74 = metadata !{i32 721152, metadata !71, metadata !"pirp__IoStatus__Status", metadata !12, i32 700, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!75 = metadata !{i32 718, i32 5, metadata !76, null}
!76 = metadata !{i32 720907, metadata !73, i32 711, i32 20, metadata !12, i32 78} ; [ DW_TAG_lexical_block ]
!77 = metadata !{i32 -1073741637}                 
!78 = metadata !{i32 721, i32 7, metadata !79, null}
!79 = metadata !{i32 720907, metadata !76, i32 720, i32 26, metadata !12, i32 79} ; [ DW_TAG_lexical_block ]
!80 = metadata !{i32 735, i32 17, metadata !76, null}
!81 = metadata !{i32 721152, metadata !71, metadata !"tmp_ndt_1", metadata !12, i32 703, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!82 = metadata !{i32 736, i32 5, metadata !76, null}
!83 = metadata !{i32 738, i32 19, metadata !84, null}
!84 = metadata !{i32 720907, metadata !76, i32 738, i32 12, metadata !12, i32 82} ; [ DW_TAG_lexical_block ]
!85 = metadata !{i32 721152, metadata !71, metadata !"tmp_ndt_2", metadata !12, i32 704, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!86 = metadata !{i32 739, i32 7, metadata !84, null}
!87 = metadata !{i32 741, i32 21, metadata !88, null}
!88 = metadata !{i32 720907, metadata !84, i32 741, i32 14, metadata !12, i32 84} ; [ DW_TAG_lexical_block ]
!89 = metadata !{i32 721152, metadata !71, metadata !"tmp_ndt_3", metadata !12, i32 705, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!90 = metadata !{i32 742, i32 9, metadata !88, null}
!91 = metadata !{i32 744, i32 23, metadata !92, null}
!92 = metadata !{i32 720907, metadata !88, i32 744, i32 16, metadata !12, i32 86} ; [ DW_TAG_lexical_block ]
!93 = metadata !{i32 721152, metadata !71, metadata !"tmp_ndt_4", metadata !12, i32 706, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!94 = metadata !{i32 745, i32 11, metadata !92, null}
!95 = metadata !{i32 721152, metadata !96, metadata !"disketteExtension__HoldNewRequests", metadata !12, i32 1196, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!96 = metadata !{i32 720907, metadata !38, i32 1196, i32 1, metadata !12, i32 187} ; [ DW_TAG_lexical_block ]
!97 = metadata !{i32 1196, i32 7, metadata !96, metadata !98}
!98 = metadata !{i32 761, i32 24, metadata !99, null}
!99 = metadata !{i32 720907, metadata !100, i32 748, i32 20, metadata !12, i32 89} ; [ DW_TAG_lexical_block ]
!100 = metadata !{i32 720907, metadata !92, i32 747, i32 18, metadata !12, i32 88} ; [ DW_TAG_lexical_block ]
!101 = metadata !{i32 721152, metadata !96, metadata !"disketteExtension__IsRemoved", metadata !12, i32 1197, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!102 = metadata !{i32 1197, i32 7, metadata !96, metadata !98}
!103 = metadata !{i32 721152, metadata !96, metadata !"disketteExtension__IsStarted", metadata !12, i32 1199, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!104 = metadata !{i32 1199, i32 7, metadata !96, metadata !98}
!105 = metadata !{i32 721152, metadata !96, metadata !"Irp__CurrentLocation", metadata !12, i32 1200, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!106 = metadata !{i32 1200, i32 7, metadata !96, metadata !98}
!107 = metadata !{i32 721152, metadata !96, metadata !"Irp__Tail__Overlay__CurrentStackLocation", metadata !12, i32 1201, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!108 = metadata !{i32 1201, i32 7, metadata !96, metadata !98}
!109 = metadata !{i32 721152, metadata !96, metadata !"irpSp__Parameters__DeviceIoControl__OutputBufferLength", metadata !12, i32 1203, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!110 = metadata !{i32 1203, i32 7, metadata !96, metadata !98}
!111 = metadata !{i32 721152, metadata !96, metadata !"sizeof__MOUNTDEV_NAME", metadata !12, i32 1204, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!112 = metadata !{i32 1204, i32 7, metadata !96, metadata !98}
!113 = metadata !{i32 721152, metadata !96, metadata !"disketteExtension__DeviceName__Length", metadata !12, i32 1207, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!114 = metadata !{i32 1207, i32 7, metadata !96, metadata !98}
!115 = metadata !{i32 721152, metadata !96, metadata !"sizeof__USHORT", metadata !12, i32 1208, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!116 = metadata !{i32 1208, i32 7, metadata !96, metadata !98}
!117 = metadata !{i32 721152, metadata !96, metadata !"disketteExtension__InterfaceString__Buffer", metadata !12, i32 1209, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!118 = metadata !{i32 1209, i32 7, metadata !96, metadata !98}
!119 = metadata !{i32 721152, metadata !96, metadata !"disketteExtension__InterfaceString__Length", metadata !12, i32 1211, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!120 = metadata !{i32 1211, i32 7, metadata !96, metadata !98}
!121 = metadata !{i32 721152, metadata !96, metadata !"sizeof__MOUNTDEV_UNIQUE_ID", metadata !12, i32 1212, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!122 = metadata !{i32 1212, i32 7, metadata !96, metadata !98}
!123 = metadata !{i32 721152, metadata !96, metadata !"irpSp__Parameters__DeviceIoControl__InputBufferLength", metadata !12, i32 1213, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!124 = metadata !{i32 1213, i32 7, metadata !96, metadata !98}
!125 = metadata !{i32 721152, metadata !96, metadata !"sizeof__FORMAT_PARAMETERS", metadata !12, i32 1214, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!126 = metadata !{i32 1214, i32 7, metadata !96, metadata !98}
!127 = metadata !{i32 721152, metadata !96, metadata !"irpSp__Parameters__DeviceIoControl__IoControlCode___1", metadata !12, i32 1215, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!128 = metadata !{i32 1215, i32 7, metadata !96, metadata !98}
!129 = metadata !{i32 721152, metadata !96, metadata !"sizeof__FORMAT_EX_PARAMETERS", metadata !12, i32 1216, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!130 = metadata !{i32 1216, i32 7, metadata !96, metadata !98}
!131 = metadata !{i32 721152, metadata !96, metadata !"formatExParameters__FormatGapLength", metadata !12, i32 1217, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!132 = metadata !{i32 1217, i32 7, metadata !96, metadata !98}
!133 = metadata !{i32 721152, metadata !96, metadata !"formatExParameters__SectorsPerTrack", metadata !12, i32 1218, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!134 = metadata !{i32 1218, i32 7, metadata !96, metadata !98}
!135 = metadata !{i32 721152, metadata !96, metadata !"sizeof__DISK_GEOMETRY", metadata !12, i32 1219, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!136 = metadata !{i32 1219, i32 7, metadata !96, metadata !98}
!137 = metadata !{i32 721152, metadata !96, metadata !"lowestDriveMediaType", metadata !12, i32 1224, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!138 = metadata !{i32 1224, i32 7, metadata !96, metadata !98}
!139 = metadata !{i32 721152, metadata !96, metadata !"highestDriveMediaType", metadata !12, i32 1225, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!140 = metadata !{i32 1225, i32 7, metadata !96, metadata !98}
!141 = metadata !{i32 721152, metadata !96, metadata !"formatExParametersSize", metadata !12, i32 1226, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!142 = metadata !{i32 1226, i32 7, metadata !96, metadata !98}
!143 = metadata !{i32 721152, metadata !96, metadata !"__cil_tmp41", metadata !12, i32 1232, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!144 = metadata !{i32 1232, i32 7, metadata !96, metadata !98}
!145 = metadata !{i32 721152, metadata !96, metadata !"__cil_tmp44", metadata !12, i32 1233, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!146 = metadata !{i32 1233, i32 7, metadata !96, metadata !98}
!147 = metadata !{i32 721152, metadata !96, metadata !"__cil_tmp45", metadata !12, i32 1234, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!148 = metadata !{i32 1234, i32 7, metadata !96, metadata !98}
!149 = metadata !{i32 721152, metadata !96, metadata !"__cil_tmp50", metadata !12, i32 1235, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!150 = metadata !{i32 1235, i32 7, metadata !96, metadata !98}
!151 = metadata !{i32 721152, metadata !96, metadata !"__cil_tmp55", metadata !12, i32 1236, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!152 = metadata !{i32 1236, i32 7, metadata !96, metadata !98}
!153 = metadata !{i32 721152, metadata !96, metadata !"__cil_tmp60", metadata !12, i32 1237, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!154 = metadata !{i32 1237, i32 7, metadata !96, metadata !98}
!155 = metadata !{i32 721152, metadata !96, metadata !"__cil_tmp65", metadata !12, i32 1238, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!156 = metadata !{i32 1238, i32 7, metadata !96, metadata !98}
!157 = metadata !{i32 721152, metadata !96, metadata !"__cil_tmp66", metadata !12, i32 1239, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!158 = metadata !{i32 1239, i32 7, metadata !96, metadata !98}
!159 = metadata !{i32 721152, metadata !96, metadata !"__cil_tmp69", metadata !12, i32 1240, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!160 = metadata !{i32 1240, i32 7, metadata !96, metadata !98}
!161 = metadata !{i32 721152, metadata !96, metadata !"__cil_tmp72", metadata !12, i32 1241, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!162 = metadata !{i32 1241, i32 7, metadata !96, metadata !98}
!163 = metadata !{i32 721152, metadata !96, metadata !"__cil_tmp75", metadata !12, i32 1242, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!164 = metadata !{i32 1242, i32 7, metadata !96, metadata !98}
!165 = metadata !{i32 721152, metadata !96, metadata !"__cil_tmp78", metadata !12, i32 1243, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!166 = metadata !{i32 1243, i32 7, metadata !96, metadata !98}
!167 = metadata !{i32 721152, metadata !96, metadata !"__cil_tmp81", metadata !12, i32 1244, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!168 = metadata !{i32 1244, i32 7, metadata !96, metadata !98}
!169 = metadata !{i32 721152, metadata !96, metadata !"__cil_tmp88", metadata !12, i32 1247, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!170 = metadata !{i32 1247, i32 7, metadata !96, metadata !98}
!171 = metadata !{i32 1234, i32 3, metadata !172, metadata !98}
!172 = metadata !{i32 720907, metadata !96, i32 1252, i32 3, metadata !12, i32 188} ; [ DW_TAG_lexical_block ]
!173 = metadata !{i32 1000, i32 16, metadata !174, metadata !176}
!174 = metadata !{i32 720907, metadata !175, i32 986, i32 3, metadata !12, i32 139} ; [ DW_TAG_lexical_block ]
!175 = metadata !{i32 720907, metadata !30, i32 979, i32 1, metadata !12, i32 138} ; [ DW_TAG_lexical_block ]
!176 = metadata !{i32 1268, i32 11, metadata !177, metadata !98}
!177 = metadata !{i32 720907, metadata !172, i32 1257, i32 39, metadata !12, i32 192} ; [ DW_TAG_lexical_block ]
!178 = metadata !{i32 721152, metadata !175, metadata !"tmp_ndt_10", metadata !12, i32 983, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!179 = metadata !{i32 1001, i32 3, metadata !174, metadata !176}
!180 = metadata !{i32 1003, i32 18, metadata !181, metadata !176}
!181 = metadata !{i32 720907, metadata !174, i32 1003, i32 10, metadata !12, i32 143} ; [ DW_TAG_lexical_block ]
!182 = metadata !{i32 721152, metadata !175, metadata !"tmp_ndt_11", metadata !12, i32 984, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!183 = metadata !{i32 1004, i32 5, metadata !181, metadata !176}
!184 = metadata !{i32 721152, metadata !185, metadata !"Irp__Tail__Overlay__CurrentStackLocation", metadata !12, i32 172, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!185 = metadata !{i32 720907, metadata !17, i32 171, i32 1, metadata !12, i32 12} ; [ DW_TAG_lexical_block ]
!186 = metadata !{i32 172, i32 7, metadata !185, metadata !187}
!187 = metadata !{i32 766, i32 24, metadata !99, null}
!188 = metadata !{i32 721152, metadata !185, metadata !"Irp__CurrentLocation", metadata !12, i32 175, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!189 = metadata !{i32 175, i32 7, metadata !185, metadata !187}
!190 = metadata !{i32 721152, metadata !185, metadata !"disketteExtension__IsRemoved", metadata !12, i32 176, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!191 = metadata !{i32 176, i32 7, metadata !185, metadata !187}
!192 = metadata !{i32 721152, metadata !185, metadata !"disketteExtension__IsStarted", metadata !12, i32 177, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!193 = metadata !{i32 177, i32 7, metadata !185, metadata !187}
!194 = metadata !{i32 721152, metadata !185, metadata !"disketteExtension__InterfaceString__Buffer", metadata !12, i32 181, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!195 = metadata !{i32 181, i32 7, metadata !185, metadata !187}
!196 = metadata !{i32 721152, metadata !185, metadata !"disketteExtension__ArcName__Length", metadata !12, i32 183, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!197 = metadata !{i32 183, i32 7, metadata !185, metadata !187}
!198 = metadata !{i32 721152, metadata !185, metadata !"irpSp__MinorFunction", metadata !12, i32 185, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!199 = metadata !{i32 185, i32 7, metadata !185, metadata !187}
!200 = metadata !{i32 721152, metadata !185, metadata !"IoGetConfigurationInformation__FloppyCount", metadata !12, i32 186, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!201 = metadata !{i32 186, i32 7, metadata !185, metadata !187}
!202 = metadata !{i32 721152, metadata !185, metadata !"ntStatus", metadata !12, i32 189, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!203 = metadata !{i32 199, i32 3, metadata !204, metadata !187}
!204 = metadata !{i32 720907, metadata !185, i32 200, i32 3, metadata !12, i32 13} ; [ DW_TAG_lexical_block ]
!205 = metadata !{i32 721152, metadata !185, metadata !"irpSp", metadata !12, i32 187, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!206 = metadata !{i32 207, i32 3, metadata !204, metadata !187}
!207 = metadata !{i32 208, i32 3, metadata !204, metadata !187}
!208 = metadata !{i32 721152, metadata !209, metadata !"Irp__Tail__Overlay__CurrentStackLocation", metadata !12, i32 465, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!209 = metadata !{i32 720907, metadata !18, i32 464, i32 1, metadata !12, i32 48} ; [ DW_TAG_lexical_block ]
!210 = metadata !{i32 465, i32 7, metadata !209, metadata !211}
!211 = metadata !{i32 245, i32 18, metadata !212, metadata !187}
!212 = metadata !{i32 720907, metadata !213, i32 242, i32 12, metadata !12, i32 23} ; [ DW_TAG_lexical_block ]
!213 = metadata !{i32 720907, metadata !204, i32 239, i32 10, metadata !12, i32 22} ; [ DW_TAG_lexical_block ]
!214 = metadata !{i32 721152, metadata !209, metadata !"fdcInfo__AcpiBios", metadata !12, i32 482, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!215 = metadata !{i32 482, i32 7, metadata !209, metadata !211}
!216 = metadata !{i32 721152, metadata !209, metadata !"fdcInfo__AcpiFdiSupported", metadata !12, i32 483, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!217 = metadata !{i32 483, i32 7, metadata !209, metadata !211}
!218 = metadata !{i32 721152, metadata !209, metadata !"KUSER_SHARED_DATA__AlternativeArchitecture_NEC98x86", metadata !12, i32 500, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!219 = metadata !{i32 500, i32 7, metadata !209, metadata !211}
!220 = metadata !{i32 721152, metadata !209, metadata !"Dc", metadata !12, i32 489, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!221 = metadata !{i32 505, i32 3, metadata !222, metadata !211}
!222 = metadata !{i32 720907, metadata !209, i32 504, i32 3, metadata !12, i32 49} ; [ DW_TAG_lexical_block ]
!223 = metadata !{i32 721152, metadata !209, metadata !"Fp", metadata !12, i32 490, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!224 = metadata !{i32 506, i32 3, metadata !222, metadata !211}
!225 = metadata !{i32 721152, metadata !209, metadata !"irpSp", metadata !12, i32 492, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!226 = metadata !{i32 508, i32 3, metadata !222, metadata !211}
!227 = metadata !{i32 721152, metadata !209, metadata !"irpSp___0", metadata !12, i32 493, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!228 = metadata !{i32 509, i32 3, metadata !222, metadata !211}
!229 = metadata !{i32 721152, metadata !209, metadata !"nextIrpSp__Control", metadata !12, i32 495, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!230 = metadata !{i32 511, i32 3, metadata !222, metadata !211}
!231 = metadata !{i32 224}
!232 = metadata !{i32 721152, metadata !209, metadata !"irpSp__Control", metadata !12, i32 497, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!233 = metadata !{i32 528, i32 3, metadata !222, metadata !211}
!234 = metadata !{i32 1}
!235 = metadata !{i32 721153, metadata !32, metadata !"Increment", metadata !12, i32 33555495, metadata !15, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!236 = metadata !{i32 1063, i32 32, metadata !32, metadata !237}
!237 = metadata !{i32 636, i32 3, metadata !238, metadata !240}
!238 = metadata !{i32 720907, metadata !239, i32 634, i32 3, metadata !12, i32 67} ; [ DW_TAG_lexical_block ]
!239 = metadata !{i32 720907, metadata !19, i32 632, i32 1, metadata !12, i32 66} ; [ DW_TAG_lexical_block ]
!240 = metadata !{i32 987, i32 22, metadata !241, metadata !242}
!241 = metadata !{i32 720907, metadata !174, i32 985, i32 23, metadata !12, i32 140} ; [ DW_TAG_lexical_block ]
!242 = metadata !{i32 532, i32 14, metadata !222, metadata !211}
!243 = metadata !{i32 721153, metadata !32, metadata !"Wait", metadata !12, i32 50332711, metadata !15, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!244 = metadata !{i32 1063, i32 48, metadata !32, metadata !237}
!245 = metadata !{i32 -1073741802}                ; [ DW_TAG_typedef ]
!246 = metadata !{i32 721152, metadata !175, metadata !"compRetStatus1", metadata !12, i32 980, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!247 = metadata !{i64 -1073741802}                
!248 = metadata !{i32 721152, metadata !175, metadata !"__cil_tmp7", metadata !12, i32 982, metadata !249, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!249 = metadata !{i32 720932, null, metadata !"long unsigned int", null, i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!250 = metadata !{i32 989, i32 5, metadata !241, metadata !242}
!251 = metadata !{i32 1000, i32 16, metadata !174, metadata !242}
!252 = metadata !{i32 1001, i32 3, metadata !174, metadata !242}
!253 = metadata !{i32 1003, i32 18, metadata !181, metadata !242}
!254 = metadata !{i32 1004, i32 5, metadata !181, metadata !242}
!255 = metadata !{i32 259}
!256 = metadata !{i32 721152, metadata !209, metadata !"ntStatus", metadata !12, i32 475, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!257 = metadata !{i32 721153, metadata !33, metadata !"WaitReason", metadata !12, i32 33555503, metadata !15, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!258 = metadata !{i32 1071, i32 44, metadata !33, metadata !259}
!259 = metadata !{i32 536, i32 16, metadata !260, metadata !211}
!260 = metadata !{i32 720907, metadata !222, i32 534, i32 28, metadata !12, i32 53} ; [ DW_TAG_lexical_block ]
!261 = metadata !{i32 721153, metadata !33, metadata !"WaitMode", metadata !12, i32 50332719, metadata !15, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!262 = metadata !{i32 1071, i32 61, metadata !33, metadata !259}
!263 = metadata !{i32 721153, metadata !33, metadata !"Alertable", metadata !12, i32 67109935, metadata !15, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!264 = metadata !{i32 1071, i32 76, metadata !33, metadata !259}
!265 = metadata !{i32 721153, metadata !33, metadata !"Timeout", metadata !12, i32 83887152, metadata !15, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!266 = metadata !{i32 1072, i32 31, metadata !33, metadata !259}
!267 = metadata !{i32 1099, i32 16, metadata !268, metadata !259}
!268 = metadata !{i32 720907, metadata !269, i32 1075, i32 3, metadata !12, i32 161} ; [ DW_TAG_lexical_block ]
!269 = metadata !{i32 720907, metadata !33, i32 1073, i32 1, metadata !12, i32 160} ; [ DW_TAG_lexical_block ]
!270 = metadata !{i32 721152, metadata !269, metadata !"tmp_ndt_12", metadata !12, i32 1073, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!271 = metadata !{i32 1100, i32 3, metadata !268, metadata !259}
!272 = metadata !{i32 721152, metadata !209, metadata !"fdcInfo__BufferCount", metadata !12, i32 479, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!273 = metadata !{i32 543, i32 3, metadata !222, metadata !211}
!274 = metadata !{i32 721152, metadata !209, metadata !"fdcInfo__BufferSize", metadata !12, i32 480, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!275 = metadata !{i32 544, i32 3, metadata !222, metadata !211}
!276 = metadata !{i32 721152, metadata !277, metadata !"irp__Tail__Overlay__CurrentStackLocation", metadata !12, i32 647, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!277 = metadata !{i32 720907, metadata !20, i32 642, i32 1, metadata !12, i32 68} ; [ DW_TAG_lexical_block ]
!278 = metadata !{i32 647, i32 7, metadata !277, metadata !279}
!279 = metadata !{i32 545, i32 14, metadata !222, metadata !211}
!280 = metadata !{i32 721152, metadata !281, metadata !"malloc", metadata !12, i32 855, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!281 = metadata !{i32 720907, metadata !24, i32 855, i32 1, metadata !12, i32 109} ; [ DW_TAG_lexical_block ]
!282 = metadata !{i32 855, i32 7, metadata !281, metadata !283}
!283 = metadata !{i32 652, i32 9, metadata !284, metadata !279}
!284 = metadata !{i32 720907, metadata !277, i32 651, i32 3, metadata !12, i32 69} ; [ DW_TAG_lexical_block ]
!285 = metadata !{i32 721153, metadata !24, metadata !"InputBuffer", metadata !12, i32 50332500, metadata !15, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!286 = metadata !{i32 852, i32 78, metadata !24, metadata !283}
!287 = metadata !{i32 721153, metadata !24, metadata !"InputBufferLength", metadata !12, i32 67109717, metadata !15, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!288 = metadata !{i32 853, i32 39, metadata !24, metadata !283}
!289 = metadata !{i32 721153, metadata !24, metadata !"OutputBuffer", metadata !12, i32 83886933, metadata !15, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!290 = metadata !{i32 853, i32 63, metadata !24, metadata !283}
!291 = metadata !{i32 721153, metadata !24, metadata !"OutputBufferLength", metadata !12, i32 100664149, metadata !15, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!292 = metadata !{i32 853, i32 82, metadata !24, metadata !283}
!293 = metadata !{i32 721153, metadata !24, metadata !"InternalDeviceIoControl", metadata !12, i32 117441366, metadata !15, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!294 = metadata !{i32 854, i32 39, metadata !24, metadata !283}
!295 = metadata !{i32 861, i32 15, metadata !296, metadata !283}
!296 = metadata !{i32 720907, metadata !281, i32 858, i32 3, metadata !12, i32 110} ; [ DW_TAG_lexical_block ]
!297 = metadata !{i32 721152, metadata !281, metadata !"tmp_ndt_5", metadata !12, i32 856, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!298 = metadata !{i32 721152, metadata !277, metadata !"irp", metadata !12, i32 643, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!299 = metadata !{i32 862, i32 3, metadata !296, metadata !283}
!300 = metadata !{i32 968, i32 3, metadata !301, metadata !303}
!301 = metadata !{i32 720907, metadata !302, i32 967, i32 3, metadata !12, i32 135} ; [ DW_TAG_lexical_block ]
!302 = metadata !{i32 720907, metadata !29, i32 965, i32 1, metadata !12, i32 134} ; [ DW_TAG_lexical_block ]
!303 = metadata !{i32 991, i32 7, metadata !304, metadata !305}
!304 = metadata !{i32 720907, metadata !241, i32 989, i32 45, metadata !12, i32 141} ; [ DW_TAG_lexical_block ]
!305 = metadata !{i32 663, i32 14, metadata !284, metadata !279}
!306 = metadata !{i32 1001, i32 3, metadata !174, metadata !305}
!307 = metadata !{i32 1004, i32 5, metadata !181, metadata !305}
!308 = metadata !{i32 1016, i32 9, metadata !309, metadata !305}
!309 = metadata !{i32 720907, metadata !310, i32 1007, i32 14, metadata !12, i32 146} ; [ DW_TAG_lexical_block ]
!310 = metadata !{i32 720907, metadata !181, i32 1006, i32 12, metadata !12, i32 145} ; [ DW_TAG_lexical_block ]
!311 = metadata !{i32 1028, i32 5, metadata !312, metadata !305}
!312 = metadata !{i32 720907, metadata !174, i32 1027, i32 18, metadata !12, i32 149} ; [ DW_TAG_lexical_block ]
!313 = metadata !{i32 1036, i32 3, metadata !174, metadata !305}
!314 = metadata !{i32 665, i32 3, metadata !284, metadata !279}
!315 = metadata !{i32 1100, i32 3, metadata !268, metadata !316}
!316 = metadata !{i32 667, i32 5, metadata !317, metadata !279}
!317 = metadata !{i32 720907, metadata !284, i32 665, i32 28, metadata !12, i32 71} ; [ DW_TAG_lexical_block ]
!318 = metadata !{i32 548, i32 3, metadata !222, metadata !211}
!319 = metadata !{i32 550, i32 5, metadata !320, metadata !211}
!320 = metadata !{i32 720907, metadata !222, i32 548, i32 22, metadata !12, i32 54} ; [ DW_TAG_lexical_block ]
!321 = metadata !{i32 595, i32 5, metadata !320, metadata !211}
!322 = metadata !{i32 928, i32 3, metadata !323, metadata !325}
!323 = metadata !{i32 720907, metadata !324, i32 925, i32 3, metadata !12, i32 125} ; [ DW_TAG_lexical_block ]
!324 = metadata !{i32 720907, metadata !27, i32 923, i32 1, metadata !12, i32 124} ; [ DW_TAG_lexical_block ]
!325 = metadata !{i32 604, i32 19, metadata !326, metadata !211}
!326 = metadata !{i32 720907, metadata !320, i32 595, i32 24, metadata !12, i32 62} ; [ DW_TAG_lexical_block ]
!327 = metadata !{i32 949, i32 3, metadata !328, metadata !330}
!328 = metadata !{i32 720907, metadata !329, i32 946, i32 3, metadata !12, i32 130} ; [ DW_TAG_lexical_block ]
!329 = metadata !{i32 720907, metadata !28, i32 944, i32 1, metadata !12, i32 129} ; [ DW_TAG_lexical_block ]
!330 = metadata !{i32 609, i32 21, metadata !331, metadata !211}
!331 = metadata !{i32 720907, metadata !326, i32 607, i32 27, metadata !12, i32 65} ; [ DW_TAG_lexical_block ]
!332 = metadata !{i32 -1073741670}                
!333 = metadata !{i32 721152, metadata !209, metadata !"Irp__IoStatus__Status", metadata !12, i32 466, metadata !15, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!334 = metadata !{i32 624, i32 3, metadata !222, metadata !211}
!335 = metadata !{i32 721153, metadata !31, metadata !"PriorityBoost", metadata !12, i32 33555481, metadata !15, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!336 = metadata !{i32 1049, i32 39, metadata !31, metadata !337}
!337 = metadata !{i32 626, i32 3, metadata !222, metadata !211}
!338 = metadata !{i32 1053, i32 3, metadata !339, metadata !337}
!339 = metadata !{i32 720907, metadata !340, i32 1052, i32 3, metadata !12, i32 155} ; [ DW_TAG_lexical_block ]
!340 = metadata !{i32 720907, metadata !31, i32 1050, i32 1, metadata !12, i32 154} ; [ DW_TAG_lexical_block ]
!341 = metadata !{i32 798, i32 3, metadata !73, null}
!342 = metadata !{i32 801, i32 5, metadata !343, null}
!343 = metadata !{i32 720907, metadata !73, i32 798, i32 22, metadata !12, i32 97} ; [ DW_TAG_lexical_block ]
!344 = metadata !{i32 826, i32 9, metadata !345, null}
!345 = metadata !{i32 720907, metadata !346, i32 825, i32 20, metadata !12, i32 106} ; [ DW_TAG_lexical_block ]
!346 = metadata !{i32 720907, metadata !343, i32 801, i32 23, metadata !12, i32 98} ; [ DW_TAG_lexical_block ]
!347 = metadata !{i32 829, i32 9, metadata !348, null}
!348 = metadata !{i32 720907, metadata !345, i32 826, i32 28, metadata !12, i32 107} ; [ DW_TAG_lexical_block ]
!349 = metadata !{i32 834, i32 7, metadata !346, null}
!350 = metadata !{i32 851, i32 1, metadata !71, null}
