; ModuleID = './tests/bench2/ssh-simplified/s3_srvr_10_BUG.cil.co.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

declare i32 @nondet_int(...)

define i32 @main() nounwind uwtable {
entry:
  %0 = tail call i32 @nondet_1() nounwind
  %cmp.i = icmp eq i32 %0, 0
  br i1 %cmp.i, label %ssl3_accept.exit, label %switch_1_8466.i

switch_1_8466.i:                                  ; preds = %entry
  %call163.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp167.i = icmp slt i32 %call163.i, 1
  br i1 %cmp167.i, label %ssl3_accept.exit, label %ERROR.i

ERROR.i:                                          ; preds = %switch_1_8466.i
  ret i32 5

ssl3_accept.exit:                                 ; preds = %switch_1_8466.i, %entry
  br label %_UFO__exit

_UFO__exit:                                       ; preds = %_UFO__exit, %ssl3_accept.exit
  br label %_UFO__exit
}

declare i32 @nondet_1() readnone

declare i32 @exit(i32)
