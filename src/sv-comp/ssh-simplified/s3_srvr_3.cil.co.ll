; ModuleID = './tests/bench2/ssh-simplified/s3_srvr_3.cil.co.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

declare i32 @nondet_int(...)

define i32 @main() nounwind uwtable {
entry:
  %0 = tail call i32 @nondet_1() nounwind
  %call.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp.i = icmp eq i32 %0, 0
  br i1 %cmp.i, label %ssl3_accept.exit, label %while.body.i.preheader

while.body.i.preheader:                           ; preds = %entry
  %tobool231.i = icmp eq i32 %0, -256
  br label %while.body.i

NewDefault110:                                    ; preds = %LeafBlock111, %LeafBlock113
  br label %while.body.i

while.body.i:                                     ; preds = %if.end390.i, %if.end373.i, %if.end366.i, %switch_1_8609.i, %switch_1_8593.i, %if.else341.i, %if.end337.i, %switch_1_8561.i, %_L___1.i, %_L___0.i, %if.else233.i, %switch_1_8513.i, %if.end225.i, %switch_1_8466.i, %switch_1_8481.i, %if.then120.i, %LeafBlock59, %NewDefault110, %while.body.i.preheader
  %1 = phi i32 [ 0, %while.body.i.preheader ], [ %6, %if.end373.i ], [ %5, %if.end366.i ], [ %4, %if.end225.i ], [ %1, %switch_1_8481.i ], [ %3, %switch_1_8466.i ], [ %1, %switch_1_8513.i ], [ %1, %if.else233.i ], [ %1, %_L___0.i ], [ %1, %_L___1.i ], [ %1, %switch_1_8561.i ], [ %1, %if.then120.i ], [ %1, %if.else341.i ], [ %1, %if.end337.i ], [ %1, %switch_1_8593.i ], [ %1, %switch_1_8609.i ], [ %1, %LeafBlock59 ], [ %1, %NewDefault110 ], [ %1, %if.end390.i ]
  %.926 = phi i32 [ %0, %while.body.i.preheader ], [ %.926, %if.end373.i ], [ %.926, %if.end366.i ], [ %.926, %if.end225.i ], [ 8482, %switch_1_8481.i ], [ %.926, %switch_1_8466.i ], [ %.926, %switch_1_8513.i ], [ %.926, %if.else233.i ], [ %.926, %_L___0.i ], [ 8576, %_L___1.i ], [ 8576, %switch_1_8561.i ], [ %.926, %if.then120.i ], [ %.926, %if.else341.i ], [ %.926, %if.end337.i ], [ %.926, %switch_1_8593.i ], [ %.926, %switch_1_8609.i ], [ %.926, %LeafBlock59 ], [ %.926, %NewDefault110 ], [ 8640, %if.end390.i ]
  %2 = phi i32 [ 8464, %while.body.i.preheader ], [ 8672, %if.end373.i ], [ 3, %if.end366.i ], [ 8656, %if.end225.i ], [ 8448, %switch_1_8481.i ], [ 8496, %switch_1_8466.i ], [ 8528, %switch_1_8513.i ], [ 8528, %if.else233.i ], [ 8544, %_L___0.i ], [ 8448, %_L___1.i ], [ 8448, %switch_1_8561.i ], [ %.926, %if.then120.i ], [ 8592, %if.else341.i ], [ 8466, %if.end337.i ], [ 8608, %switch_1_8593.i ], [ 8640, %switch_1_8609.i ], [ 3, %LeafBlock59 ], [ 8560, %NewDefault110 ], [ 8448, %if.end390.i ]
  br label %NodeBlock100

NodeBlock100:                                     ; preds = %while.body.i
  %Pivot101 = icmp slt i32 %2, 8512
  br i1 %Pivot101, label %NodeBlock68, label %NodeBlock98

NodeBlock98:                                      ; preds = %NodeBlock100
  %Pivot99 = icmp slt i32 %2, 8560
  br i1 %Pivot99, label %NodeBlock81, label %NodeBlock96

NodeBlock96:                                      ; preds = %NodeBlock98
  %Pivot97 = icmp slt i32 %2, 16384
  br i1 %Pivot97, label %NodeBlock88, label %NodeBlock94

NodeBlock94:                                      ; preds = %NodeBlock96
  %Pivot95 = icmp slt i32 %2, 24576
  br i1 %Pivot95, label %LeafBlock90, label %LeafBlock92

LeafBlock92:                                      ; preds = %NodeBlock94
  %SwitchLeaf93 = icmp eq i32 %2, 24576
  br i1 %SwitchLeaf93, label %ssl3_accept.exit.loopexit, label %NewDefault

LeafBlock90:                                      ; preds = %NodeBlock94
  %SwitchLeaf91 = icmp eq i32 %2, 16384
  br i1 %SwitchLeaf91, label %ssl3_accept.exit.loopexit, label %NewDefault

NodeBlock88:                                      ; preds = %NodeBlock96
  %Pivot89 = icmp slt i32 %2, 12292
  br i1 %Pivot89, label %LeafBlock83, label %LeafBlock86

LeafBlock86:                                      ; preds = %NodeBlock88
  %SwitchLeaf87 = icmp eq i32 %2, 12292
  br i1 %SwitchLeaf87, label %ssl3_accept.exit.loopexit, label %NewDefault

LeafBlock83:                                      ; preds = %NodeBlock88
  %.off84 = add i32 %2, -8560
  %SwitchLeaf85 = icmp ule i32 %.off84, 1
  br i1 %SwitchLeaf85, label %switch_1_8561.i, label %NewDefault

NodeBlock81:                                      ; preds = %NodeBlock98
  %Pivot82 = icmp slt i32 %2, 8528
  br i1 %Pivot82, label %LeafBlock70, label %NodeBlock79

NodeBlock79:                                      ; preds = %NodeBlock81
  %Pivot80 = icmp slt i32 %2, 8544
  br i1 %Pivot80, label %LeafBlock73, label %LeafBlock76

LeafBlock76:                                      ; preds = %NodeBlock79
  %.off77 = add i32 %2, -8544
  %SwitchLeaf78 = icmp ule i32 %.off77, 1
  br i1 %SwitchLeaf78, label %switch_1_8545.i, label %NewDefault

LeafBlock73:                                      ; preds = %NodeBlock79
  %.off74 = add i32 %2, -8528
  %SwitchLeaf75 = icmp ule i32 %.off74, 1
  br i1 %SwitchLeaf75, label %_L___0.i, label %NewDefault

LeafBlock70:                                      ; preds = %NodeBlock81
  %.off71 = add i32 %2, -8512
  %SwitchLeaf72 = icmp ule i32 %.off71, 1
  br i1 %SwitchLeaf72, label %switch_1_8513.i, label %NewDefault

NodeBlock68:                                      ; preds = %NodeBlock100
  %Pivot69 = icmp slt i32 %2, 8464
  br i1 %Pivot69, label %NodeBlock49, label %NodeBlock66

NodeBlock66:                                      ; preds = %NodeBlock68
  %Pivot67 = icmp slt i32 %2, 8482
  br i1 %Pivot67, label %NodeBlock57, label %NodeBlock64

NodeBlock64:                                      ; preds = %NodeBlock66
  %Pivot65 = icmp slt i32 %2, 8496
  br i1 %Pivot65, label %LeafBlock59, label %LeafBlock61

LeafBlock61:                                      ; preds = %NodeBlock64
  %.off62 = add i32 %2, -8496
  %SwitchLeaf63 = icmp ule i32 %.off62, 1
  br i1 %SwitchLeaf63, label %switch_1_8497.i, label %NewDefault

LeafBlock59:                                      ; preds = %NodeBlock64
  %SwitchLeaf60 = icmp eq i32 %2, 8482
  br i1 %SwitchLeaf60, label %while.body.i, label %NewDefault

NodeBlock57:                                      ; preds = %NodeBlock66
  %Pivot58 = icmp slt i32 %2, 8480
  br i1 %Pivot58, label %LeafBlock51, label %LeafBlock54

LeafBlock54:                                      ; preds = %NodeBlock57
  %.off55 = add i32 %2, -8480
  %SwitchLeaf56 = icmp ule i32 %.off55, 1
  br i1 %SwitchLeaf56, label %switch_1_8481.i, label %NewDefault

LeafBlock51:                                      ; preds = %NodeBlock57
  %.off52 = add i32 %2, -8464
  %SwitchLeaf53 = icmp ule i32 %.off52, 2
  br i1 %SwitchLeaf53, label %switch_1_8466.i, label %NewDefault

NodeBlock49:                                      ; preds = %NodeBlock68
  %Pivot50 = icmp slt i32 %2, 8195
  br i1 %Pivot50, label %LeafBlock, label %NodeBlock

NodeBlock:                                        ; preds = %NodeBlock49
  %Pivot = icmp slt i32 %2, 8448
  br i1 %Pivot, label %LeafBlock45, label %LeafBlock47

LeafBlock47:                                      ; preds = %NodeBlock
  %SwitchLeaf48 = icmp eq i32 %2, 8448
  br i1 %SwitchLeaf48, label %if.then120.i, label %NewDefault

LeafBlock45:                                      ; preds = %NodeBlock
  %SwitchLeaf46 = icmp eq i32 %2, 8195
  br i1 %SwitchLeaf46, label %ssl3_accept.exit.loopexit, label %NewDefault

LeafBlock:                                        ; preds = %NodeBlock49
  %SwitchLeaf = icmp eq i32 %2, 8192
  br i1 %SwitchLeaf, label %ssl3_accept.exit.loopexit, label %NewDefault

if.then120.i:                                     ; preds = %LeafBlock47
  br label %while.body.i

NewDefault:                                       ; preds = %LeafBlock, %LeafBlock45, %LeafBlock47, %LeafBlock51, %LeafBlock54, %LeafBlock59, %LeafBlock61, %LeafBlock70, %LeafBlock73, %LeafBlock76, %LeafBlock83, %LeafBlock86, %LeafBlock90, %LeafBlock92
  br label %if.else121.i

if.else121.i:                                     ; preds = %NewDefault
  %.off = add i32 %2, -8576
  %switch = icmp ult i32 %.off, 2
  br i1 %switch, label %switch_1_8577.i, label %if.else129.i

if.else129.i:                                     ; preds = %if.else121.i
  %.off35 = add i32 %2, -8592
  %switch36 = icmp ult i32 %.off35, 2
  br i1 %switch36, label %switch_1_8593.i, label %if.else137.i

if.else137.i:                                     ; preds = %if.else129.i
  %.off37 = add i32 %2, -8608
  %switch38 = icmp ult i32 %.off37, 2
  br i1 %switch38, label %switch_1_8609.i, label %if.else145.i

if.else145.i:                                     ; preds = %if.else137.i
  %.off39 = add i32 %2, -8640
  %switch40 = icmp ult i32 %.off39, 2
  br i1 %switch40, label %switch_1_8641.i, label %if.else153.i

if.else153.i:                                     ; preds = %if.else145.i
  %.off41 = add i32 %2, -8656
  %switch42 = icmp ult i32 %.off41, 2
  br i1 %switch42, label %if.end373.i, label %if.else161.i

if.else161.i:                                     ; preds = %if.else153.i
  br label %NodeBlock108

NodeBlock108:                                     ; preds = %if.else161.i
  %Pivot109 = icmp slt i32 %2, 8672
  br i1 %Pivot109, label %LeafBlock103, label %LeafBlock105

LeafBlock105:                                     ; preds = %NodeBlock108
  %.off106 = add i32 %2, -8672
  %SwitchLeaf107 = icmp ule i32 %.off106, 1
  br i1 %SwitchLeaf107, label %switch_1_8673.i, label %NewDefault102

LeafBlock103:                                     ; preds = %NodeBlock108
  %SwitchLeaf104 = icmp eq i32 %2, 3
  br i1 %SwitchLeaf104, label %ssl3_accept.exit, label %NewDefault102

switch_1_8481.i:                                  ; preds = %LeafBlock54
  %call203.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp204.i = icmp slt i32 %call203.i, 1
  br i1 %cmp204.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8466.i:                                  ; preds = %LeafBlock51
  %call208.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp209.i = icmp eq i32 %1, 0
  %3 = select i1 %cmp209.i, i32 1, i32 %1
  %cmp213.i = icmp slt i32 %call208.i, 1
  br i1 %cmp213.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8497.i:                                  ; preds = %LeafBlock61
  %call217.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp222.i = icmp slt i32 %call217.i, 1
  br i1 %cmp222.i, label %ssl3_accept.exit.loopexit, label %if.end225.i

if.end225.i:                                      ; preds = %switch_1_8497.i
  %cmp218.i = icmp eq i32 %1, 1
  %4 = select i1 %cmp218.i, i32 2, i32 %1
  br label %while.body.i

switch_1_8513.i:                                  ; preds = %LeafBlock70
  br i1 %tobool231.i, label %if.else233.i, label %while.body.i

if.else233.i:                                     ; preds = %switch_1_8513.i
  %call234.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp235.i = icmp slt i32 %call234.i, 1
  br i1 %cmp235.i, label %ssl3_accept.exit.loopexit, label %while.body.i

_L___0.i:                                         ; preds = %LeafBlock73
  %call275.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp276.i = icmp slt i32 %call275.i, 1
  br i1 %cmp276.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8545.i:                                  ; preds = %LeafBlock76
  br label %NodeBlock115

NodeBlock115:                                     ; preds = %switch_1_8545.i
  %Pivot116 = icmp slt i32 %0, 0
  br i1 %Pivot116, label %LeafBlock111, label %LeafBlock113

LeafBlock113:                                     ; preds = %NodeBlock115
  %SwitchLeaf114 = icmp eq i32 %0, 0
  br i1 %SwitchLeaf114, label %_L___1.i, label %NewDefault110

LeafBlock111:                                     ; preds = %NodeBlock115
  %SwitchLeaf112 = icmp eq i32 %0, -4
  br i1 %SwitchLeaf112, label %_L___1.i, label %NewDefault110

_L___1.i:                                         ; preds = %LeafBlock111, %LeafBlock113
  %call311.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp312.i = icmp slt i32 %call311.i, 1
  br i1 %cmp312.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8561.i:                                  ; preds = %LeafBlock83
  %call320.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp321.i = icmp slt i32 %call320.i, 1
  br i1 %cmp321.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8577.i:                                  ; preds = %if.else121.i
  %call333.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp334.i = icmp slt i32 %call333.i, 1
  br i1 %cmp334.i, label %ssl3_accept.exit.loopexit, label %if.end337.i

if.end337.i:                                      ; preds = %switch_1_8577.i
  %cmp338.i = icmp eq i32 %call333.i, 2
  br i1 %cmp338.i, label %while.body.i, label %if.else341.i

if.else341.i:                                     ; preds = %if.end337.i
  %call342.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp343.i = icmp slt i32 %call342.i, 1
  br i1 %cmp343.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8593.i:                                  ; preds = %if.else129.i
  %call348.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp349.i = icmp slt i32 %call348.i, 1
  br i1 %cmp349.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8609.i:                                  ; preds = %if.else137.i
  %call353.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp354.i = icmp slt i32 %call353.i, 1
  br i1 %cmp354.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8641.i:                                  ; preds = %if.else145.i
  %call358.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp363.i = icmp slt i32 %call358.i, 1
  br i1 %cmp363.i, label %ssl3_accept.exit.loopexit, label %if.end366.i

if.end366.i:                                      ; preds = %switch_1_8641.i
  %cmp359.i = icmp eq i32 %1, 3
  %5 = select i1 %cmp359.i, i32 4, i32 %1
  br label %while.body.i

if.end373.i:                                      ; preds = %if.else153.i
  %call374.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp375.i = icmp eq i32 %1, 2
  %6 = select i1 %cmp375.i, i32 3, i32 %1
  %cmp379.i = icmp slt i32 %call374.i, 1
  br i1 %cmp379.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8673.i:                                  ; preds = %LeafBlock105
  %call386.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp387.i = icmp eq i32 %1, 4
  br i1 %cmp387.i, label %if.then389.i, label %if.end390.i

if.then389.i:                                     ; preds = %switch_1_8673.i
  ret i32 5

if.end390.i:                                      ; preds = %switch_1_8673.i
  %cmp391.i = icmp slt i32 %call386.i, 1
  br i1 %cmp391.i, label %ssl3_accept.exit.loopexit, label %while.body.i

NewDefault102:                                    ; preds = %LeafBlock103, %LeafBlock105
  br label %ssl3_accept.exit.loopexit

ssl3_accept.exit.loopexit:                        ; preds = %NewDefault102, %if.end390.i, %if.end373.i, %switch_1_8641.i, %switch_1_8609.i, %switch_1_8593.i, %if.else341.i, %switch_1_8577.i, %switch_1_8561.i, %_L___1.i, %_L___0.i, %if.else233.i, %switch_1_8497.i, %switch_1_8466.i, %switch_1_8481.i, %LeafBlock, %LeafBlock45, %LeafBlock86, %LeafBlock90, %LeafBlock92
  br label %ssl3_accept.exit

ssl3_accept.exit:                                 ; preds = %ssl3_accept.exit.loopexit, %LeafBlock103, %entry
  br label %_UFO__exit

_UFO__exit:                                       ; preds = %_UFO__exit, %ssl3_accept.exit
  br label %_UFO__exit
}

declare i32 @nondet_1() readnone

declare i32 @exit(i32)
