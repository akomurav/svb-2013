; ModuleID = './tests/bench2/ssh-simplified/s3_srvr_13_BUG.cil.co.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

declare i32 @nondet_int(...)

define i32 @main() nounwind uwtable {
entry:
  %0 = tail call i32 @nondet_1() nounwind
  %1 = tail call i64 @nondet_2() nounwind
  %call.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp.i = icmp eq i32 %0, 0
  br i1 %cmp.i, label %ssl3_accept.exit, label %while.body.i.preheader

while.body.i.preheader:                           ; preds = %entry
  %notrhs = icmp ne i32 %0, -2097152
  %mul279.i = shl nsw i32 %0, 3
  %tobool319.i = icmp eq i32 %0, -2
  br label %while.body.i

while.body.i:                                     ; preds = %if.then497.i, %switch_1_break.i, %switch_1_break.i.thread, %while.body.i.preheader
  %2 = phi i64 [ %1, %while.body.i.preheader ], [ %.ph36, %switch_1_break.i.thread ], [ %15, %switch_1_break.i ], [ %15, %if.then497.i ]
  %3 = phi i32 [ 0, %while.body.i.preheader ], [ %3, %switch_1_break.i.thread ], [ %16, %switch_1_break.i ], [ %16, %if.then497.i ]
  %4 = phi i32 [ %0, %while.body.i.preheader ], [ %.ph37, %switch_1_break.i.thread ], [ %17, %switch_1_break.i ], [ %17, %if.then497.i ]
  %5 = phi i64 [ %1, %while.body.i.preheader ], [ %.ph38, %switch_1_break.i.thread ], [ %18, %switch_1_break.i ], [ %18, %if.then497.i ]
  %.927 = phi i32 [ %0, %while.body.i.preheader ], [ %.927, %switch_1_break.i.thread ], [ %.928, %switch_1_break.i ], [ %.928, %if.then497.i ]
  %6 = phi i32 [ 8464, %while.body.i.preheader ], [ %.ph39, %switch_1_break.i.thread ], [ %19, %switch_1_break.i ], [ %19, %if.then497.i ]
  br label %NodeBlock123

NodeBlock123:                                     ; preds = %while.body.i
  %Pivot124 = icmp slt i32 %6, 8544
  br i1 %Pivot124, label %NodeBlock76, label %NodeBlock121

NodeBlock121:                                     ; preds = %NodeBlock123
  %Pivot122 = icmp slt i32 %6, 8640
  br i1 %Pivot122, label %NodeBlock99, label %NodeBlock119

NodeBlock119:                                     ; preds = %NodeBlock121
  %Pivot120 = icmp slt i32 %6, 12292
  br i1 %Pivot120, label %NodeBlock107, label %NodeBlock117

NodeBlock117:                                     ; preds = %NodeBlock119
  %Pivot118 = icmp slt i32 %6, 16384
  br i1 %Pivot118, label %LeafBlock109, label %NodeBlock115

NodeBlock115:                                     ; preds = %NodeBlock117
  %Pivot116 = icmp slt i32 %6, 24576
  br i1 %Pivot116, label %LeafBlock111, label %LeafBlock113

LeafBlock113:                                     ; preds = %NodeBlock115
  %SwitchLeaf114 = icmp eq i32 %6, 24576
  br i1 %SwitchLeaf114, label %ssl3_accept.exit.loopexit, label %NewDefault

LeafBlock111:                                     ; preds = %NodeBlock115
  %SwitchLeaf112 = icmp eq i32 %6, 16384
  br i1 %SwitchLeaf112, label %ssl3_accept.exit.loopexit, label %NewDefault

LeafBlock109:                                     ; preds = %NodeBlock117
  %SwitchLeaf110 = icmp eq i32 %6, 12292
  br i1 %SwitchLeaf110, label %ssl3_accept.exit.loopexit, label %NewDefault

NodeBlock107:                                     ; preds = %NodeBlock119
  %Pivot108 = icmp slt i32 %6, 8656
  br i1 %Pivot108, label %LeafBlock101, label %LeafBlock104

LeafBlock104:                                     ; preds = %NodeBlock107
  %.off105 = add i32 %6, -8656
  %SwitchLeaf106 = icmp ule i32 %.off105, 1
  br i1 %SwitchLeaf106, label %if.end390.i, label %NewDefault

LeafBlock101:                                     ; preds = %NodeBlock107
  %.off102 = add i32 %6, -8640
  %SwitchLeaf103 = icmp ule i32 %.off102, 1
  br i1 %SwitchLeaf103, label %switch_1_8641.i, label %NewDefault

NodeBlock99:                                      ; preds = %NodeBlock121
  %Pivot100 = icmp slt i32 %6, 8576
  br i1 %Pivot100, label %NodeBlock84, label %NodeBlock97

NodeBlock97:                                      ; preds = %NodeBlock99
  %Pivot98 = icmp slt i32 %6, 8592
  br i1 %Pivot98, label %LeafBlock86, label %NodeBlock95

NodeBlock95:                                      ; preds = %NodeBlock97
  %Pivot96 = icmp slt i32 %6, 8608
  br i1 %Pivot96, label %LeafBlock89, label %LeafBlock92

LeafBlock92:                                      ; preds = %NodeBlock95
  %.off93 = add i32 %6, -8608
  %SwitchLeaf94 = icmp ule i32 %.off93, 1
  br i1 %SwitchLeaf94, label %switch_1_8609.i, label %NewDefault

LeafBlock89:                                      ; preds = %NodeBlock95
  %.off90 = add i32 %6, -8592
  %SwitchLeaf91 = icmp ule i32 %.off90, 1
  br i1 %SwitchLeaf91, label %switch_1_8593.i, label %NewDefault

LeafBlock86:                                      ; preds = %NodeBlock97
  %.off87 = add i32 %6, -8576
  %SwitchLeaf88 = icmp ule i32 %.off87, 1
  br i1 %SwitchLeaf88, label %switch_1_8577.i, label %NewDefault

NodeBlock84:                                      ; preds = %NodeBlock99
  %Pivot85 = icmp slt i32 %6, 8560
  br i1 %Pivot85, label %LeafBlock78, label %LeafBlock81

LeafBlock81:                                      ; preds = %NodeBlock84
  %.off82 = add i32 %6, -8560
  %SwitchLeaf83 = icmp ule i32 %.off82, 1
  br i1 %SwitchLeaf83, label %switch_1_8561.i, label %NewDefault

LeafBlock78:                                      ; preds = %NodeBlock84
  %.off79 = add i32 %6, -8544
  %SwitchLeaf80 = icmp ule i32 %.off79, 1
  br i1 %SwitchLeaf80, label %switch_1_8545.i, label %NewDefault

NodeBlock76:                                      ; preds = %NodeBlock123
  %Pivot77 = icmp slt i32 %6, 8480
  br i1 %Pivot77, label %NodeBlock52, label %NodeBlock74

NodeBlock74:                                      ; preds = %NodeBlock76
  %Pivot75 = icmp slt i32 %6, 8496
  br i1 %Pivot75, label %NodeBlock59, label %NodeBlock72

NodeBlock72:                                      ; preds = %NodeBlock74
  %Pivot73 = icmp slt i32 %6, 8512
  br i1 %Pivot73, label %LeafBlock61, label %NodeBlock70

NodeBlock70:                                      ; preds = %NodeBlock72
  %Pivot71 = icmp slt i32 %6, 8528
  br i1 %Pivot71, label %LeafBlock64, label %LeafBlock67

LeafBlock67:                                      ; preds = %NodeBlock70
  %.off68 = add i32 %6, -8528
  %SwitchLeaf69 = icmp ule i32 %.off68, 1
  br i1 %SwitchLeaf69, label %switch_1_8529.i, label %NewDefault

LeafBlock64:                                      ; preds = %NodeBlock70
  %.off65 = add i32 %6, -8512
  %SwitchLeaf66 = icmp ule i32 %.off65, 1
  br i1 %SwitchLeaf66, label %switch_1_8513.i, label %NewDefault

LeafBlock61:                                      ; preds = %NodeBlock72
  %.off62 = add i32 %6, -8496
  %SwitchLeaf63 = icmp ule i32 %.off62, 1
  br i1 %SwitchLeaf63, label %switch_1_8497.i, label %NewDefault

NodeBlock59:                                      ; preds = %NodeBlock74
  %Pivot60 = icmp slt i32 %6, 8482
  br i1 %Pivot60, label %LeafBlock54, label %LeafBlock57

LeafBlock57:                                      ; preds = %NodeBlock59
  %SwitchLeaf58 = icmp eq i32 %6, 8482
  br i1 %SwitchLeaf58, label %switch_1_break.i, label %NewDefault

LeafBlock54:                                      ; preds = %NodeBlock59
  %.off55 = add i32 %6, -8480
  %SwitchLeaf56 = icmp ule i32 %.off55, 1
  br i1 %SwitchLeaf56, label %switch_1_8481.i, label %NewDefault

NodeBlock52:                                      ; preds = %NodeBlock76
  %Pivot53 = icmp slt i32 %6, 8448
  br i1 %Pivot53, label %NodeBlock, label %NodeBlock50

NodeBlock50:                                      ; preds = %NodeBlock52
  %Pivot51 = icmp slt i32 %6, 8464
  br i1 %Pivot51, label %LeafBlock46, label %LeafBlock48

LeafBlock48:                                      ; preds = %NodeBlock50
  %.off = add i32 %6, -8464
  %SwitchLeaf49 = icmp ule i32 %.off, 2
  br i1 %SwitchLeaf49, label %switch_1_8466.i, label %NewDefault

LeafBlock46:                                      ; preds = %NodeBlock50
  %SwitchLeaf47 = icmp eq i32 %6, 8448
  br i1 %SwitchLeaf47, label %if.then120.i, label %NewDefault

NodeBlock:                                        ; preds = %NodeBlock52
  %Pivot = icmp slt i32 %6, 8195
  br i1 %Pivot, label %LeafBlock, label %LeafBlock44

LeafBlock44:                                      ; preds = %NodeBlock
  %SwitchLeaf45 = icmp eq i32 %6, 8195
  br i1 %SwitchLeaf45, label %ssl3_accept.exit.loopexit, label %NewDefault

LeafBlock:                                        ; preds = %NodeBlock
  %SwitchLeaf = icmp eq i32 %6, 8192
  br i1 %SwitchLeaf, label %ssl3_accept.exit.loopexit, label %NewDefault

if.then120.i:                                     ; preds = %LeafBlock46
  br label %switch_1_break.i

NewDefault:                                       ; preds = %LeafBlock, %LeafBlock44, %LeafBlock46, %LeafBlock48, %LeafBlock54, %LeafBlock57, %LeafBlock61, %LeafBlock64, %LeafBlock67, %LeafBlock78, %LeafBlock81, %LeafBlock86, %LeafBlock89, %LeafBlock92, %LeafBlock101, %LeafBlock104, %LeafBlock109, %LeafBlock111, %LeafBlock113
  br label %if.else161.i

if.else161.i:                                     ; preds = %NewDefault
  br label %NodeBlock131

NodeBlock131:                                     ; preds = %if.else161.i
  %Pivot132 = icmp slt i32 %6, 8672
  br i1 %Pivot132, label %LeafBlock126, label %LeafBlock128

LeafBlock128:                                     ; preds = %NodeBlock131
  %.off129 = add i32 %6, -8672
  %SwitchLeaf130 = icmp ule i32 %.off129, 1
  br i1 %SwitchLeaf130, label %switch_1_8673.i, label %NewDefault125

LeafBlock126:                                     ; preds = %NodeBlock131
  %SwitchLeaf127 = icmp eq i32 %6, 3
  br i1 %SwitchLeaf127, label %ssl3_accept.exit, label %NewDefault125

switch_1_8481.i:                                  ; preds = %LeafBlock54
  %call203.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp204.i = icmp slt i32 %call203.i, 1
  br i1 %cmp204.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8466.i:                                  ; preds = %LeafBlock48
  %call208.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp209.i = icmp eq i32 %3, 0
  %7 = select i1 %cmp209.i, i32 1, i32 %3
  %cmp213.i = icmp slt i32 %call208.i, 1
  br i1 %cmp213.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8497.i:                                  ; preds = %LeafBlock61
  %call217.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp222.i = icmp slt i32 %call217.i, 1
  br i1 %cmp222.i, label %ssl3_accept.exit.loopexit, label %if.end225.i

if.end225.i:                                      ; preds = %switch_1_8497.i
  %cmp218.i = icmp eq i32 %3, 1
  %8 = select i1 %cmp218.i, i32 2, i32 %3
  br label %switch_1_break.i

switch_1_8513.i:                                  ; preds = %LeafBlock64
  %call230.i = tail call i32 (...)* @nondet_int() nounwind
  %tobool232.i = icmp eq i32 %call230.i, -256
  br i1 %tobool232.i, label %if.else234.i, label %switch_1_break.i.thread

if.else234.i:                                     ; preds = %switch_1_8513.i
  %call235.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp236.i = icmp eq i32 %3, 2
  %9 = select i1 %cmp236.i, i32 6, i32 %3
  %cmp240.i = icmp slt i32 %call235.i, 1
  br i1 %cmp240.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8529.i:                                  ; preds = %LeafBlock67
  %call245.i = tail call i32 (...)* @nondet_int() nounwind
  %conv246.i = sext i32 %call245.i to i64
  %notlhs = icmp ne i32 %call245.i, -30
  %or.cond.not = or i1 %notrhs, %notlhs
  br i1 %or.cond.not, label %_L___0.i, label %if.else266.i

if.else266.i:                                     ; preds = %switch_1_8529.i
  %call267.i = tail call i32 (...)* @nondet_int() nounwind
  %conv268.i = sext i32 %call267.i to i64
  %tobool270.i = icmp eq i32 %call267.i, -2
  br i1 %tobool270.i, label %switch_1_break.i.thread, label %if.then271.i

if.then271.i:                                     ; preds = %if.else266.i
  %call272.i = tail call i32 (...)* @nondet_int() nounwind
  %tobool275.i = icmp eq i32 %call272.i, -4
  %.7 = select i1 %tobool275.i, i32 1024, i32 512
  %cmp280.i = icmp sgt i32 %mul279.i, %.7
  br i1 %cmp280.i, label %_L___0.i, label %switch_1_break.i.thread

_L___0.i:                                         ; preds = %if.then271.i, %switch_1_8529.i
  %10 = phi i64 [ %conv268.i, %if.then271.i ], [ %2, %switch_1_8529.i ]
  %11 = phi i32 [ %.7, %if.then271.i ], [ %4, %switch_1_8529.i ]
  %call283.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp284.i = icmp eq i32 %3, 6
  %12 = select i1 %cmp284.i, i32 7, i32 %3
  %cmp288.i = icmp slt i32 %call283.i, 1
  br i1 %cmp288.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8545.i:                                  ; preds = %LeafBlock78
  br label %NodeBlock138

NodeBlock138:                                     ; preds = %switch_1_8545.i
  %Pivot139 = icmp slt i32 %0, 0
  br i1 %Pivot139, label %LeafBlock134, label %LeafBlock136

LeafBlock136:                                     ; preds = %NodeBlock138
  %SwitchLeaf137 = icmp eq i32 %0, 0
  br i1 %SwitchLeaf137, label %_L___2.i, label %NewDefault133

LeafBlock134:                                     ; preds = %NodeBlock138
  %SwitchLeaf135 = icmp eq i32 %0, -4
  br i1 %SwitchLeaf135, label %_L___2.i, label %NewDefault133

_L___2.i:                                         ; preds = %LeafBlock134, %LeafBlock136
  %call313.i = tail call i32 (...)* @nondet_int() nounwind
  %tobool316.i = icmp ne i32 %call313.i, -256
  %or.cond31 = and i1 %tobool316.i, %tobool319.i
  br i1 %or.cond31, label %switch_1_break.i.thread, label %_L___1.i

_L___1.i:                                         ; preds = %_L___2.i
  %call324.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp325.i = icmp eq i32 %3, 8
  br i1 %cmp325.i, label %ERROR.i, label %if.end328.i

if.end328.i:                                      ; preds = %_L___1.i
  %cmp329.i = icmp slt i32 %call324.i, 1
  br i1 %cmp329.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8561.i:                                  ; preds = %LeafBlock81
  %call337.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp338.i = icmp slt i32 %call337.i, 1
  br i1 %cmp338.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8577.i:                                  ; preds = %LeafBlock86
  %call350.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp351.i = icmp slt i32 %call350.i, 1
  br i1 %cmp351.i, label %ssl3_accept.exit.loopexit, label %if.end354.i

if.end354.i:                                      ; preds = %switch_1_8577.i
  %cmp355.i = icmp eq i32 %call350.i, 2
  br i1 %cmp355.i, label %switch_1_break.i, label %if.else358.i

if.else358.i:                                     ; preds = %if.end354.i
  %call359.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp360.i = icmp slt i32 %call359.i, 1
  br i1 %cmp360.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8593.i:                                  ; preds = %LeafBlock89
  %call365.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp366.i = icmp slt i32 %call365.i, 1
  br i1 %cmp366.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8609.i:                                  ; preds = %LeafBlock92
  %call370.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp371.i = icmp slt i32 %call370.i, 1
  br i1 %cmp371.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8641.i:                                  ; preds = %LeafBlock101
  %call375.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp376.i = icmp eq i32 %3, 5
  br i1 %cmp376.i, label %ERROR.i, label %if.end379.i

if.end379.i:                                      ; preds = %switch_1_8641.i
  %cmp380.i = icmp slt i32 %call375.i, 1
  br i1 %cmp380.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

if.end390.i:                                      ; preds = %LeafBlock104
  %call391.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp392.i = icmp eq i32 %3, 2
  br i1 %cmp392.i, label %if.end405.i, label %if.else395.i

if.else395.i:                                     ; preds = %if.end390.i
  %cmp396.i = icmp eq i32 %3, 4
  br i1 %cmp396.i, label %if.end405.i, label %if.else399.i

if.else399.i:                                     ; preds = %if.else395.i
  %cmp400.i = icmp eq i32 %3, 7
  br i1 %cmp400.i, label %if.then402.i, label %if.end405.i

if.then402.i:                                     ; preds = %if.else399.i
  br label %if.end405.i

if.end405.i:                                      ; preds = %if.then402.i, %if.else399.i, %if.else395.i, %if.end390.i
  %13 = phi i32 [ 8, %if.then402.i ], [ %3, %if.else399.i ], [ 3, %if.end390.i ], [ 5, %if.else395.i ]
  %cmp406.i = icmp slt i32 %call391.i, 1
  br i1 %cmp406.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8673.i:                                  ; preds = %LeafBlock128
  %call413.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp418.i = icmp slt i32 %call413.i, 1
  br i1 %cmp418.i, label %ssl3_accept.exit.loopexit, label %if.end421.i

if.end421.i:                                      ; preds = %switch_1_8673.i
  %cmp414.i = icmp eq i32 %3, 3
  %14 = select i1 %cmp414.i, i32 4, i32 %3
  br label %switch_1_break.i

NewDefault133:                                    ; preds = %LeafBlock134, %LeafBlock136
  br label %switch_1_break.i.thread

switch_1_break.i.thread:                          ; preds = %NewDefault133, %_L___2.i, %if.then271.i, %if.else266.i, %switch_1_8513.i
  %.ph36 = phi i64 [ %2, %switch_1_8513.i ], [ %conv268.i, %if.then271.i ], [ %conv268.i, %if.else266.i ], [ %2, %_L___2.i ], [ %2, %NewDefault133 ]
  %.ph37 = phi i32 [ %4, %switch_1_8513.i ], [ %.7, %if.then271.i ], [ %4, %if.else266.i ], [ %4, %_L___2.i ], [ %4, %NewDefault133 ]
  %.ph38 = phi i64 [ %5, %switch_1_8513.i ], [ %conv246.i, %if.then271.i ], [ -30, %if.else266.i ], [ %5, %_L___2.i ], [ %5, %NewDefault133 ]
  %.ph39 = phi i32 [ 8528, %switch_1_8513.i ], [ 8544, %if.then271.i ], [ 8544, %if.else266.i ], [ 8560, %_L___2.i ], [ 8560, %NewDefault133 ]
  %call467.i40 = tail call i32 (...)* @nondet_int() nounwind
  br label %while.body.i

switch_1_break.i:                                 ; preds = %if.end421.i, %if.end405.i, %if.end379.i, %switch_1_8609.i, %switch_1_8593.i, %if.else358.i, %if.end354.i, %switch_1_8561.i, %if.end328.i, %_L___0.i, %if.else234.i, %if.end225.i, %switch_1_8466.i, %switch_1_8481.i, %if.then120.i, %LeafBlock57
  %15 = phi i64 [ %2, %if.end421.i ], [ %2, %if.end225.i ], [ %2, %switch_1_8481.i ], [ %2, %switch_1_8466.i ], [ %2, %if.else234.i ], [ %10, %_L___0.i ], [ %2, %if.end328.i ], [ %2, %switch_1_8561.i ], [ %2, %if.then120.i ], [ %2, %if.else358.i ], [ %2, %if.end354.i ], [ %2, %switch_1_8593.i ], [ %2, %switch_1_8609.i ], [ %2, %if.end405.i ], [ %2, %LeafBlock57 ], [ %2, %if.end379.i ]
  %16 = phi i32 [ %14, %if.end421.i ], [ %8, %if.end225.i ], [ %3, %switch_1_8481.i ], [ %7, %switch_1_8466.i ], [ %9, %if.else234.i ], [ %12, %_L___0.i ], [ %3, %if.end328.i ], [ %3, %switch_1_8561.i ], [ %3, %if.then120.i ], [ %3, %if.else358.i ], [ %3, %if.end354.i ], [ %3, %switch_1_8593.i ], [ %3, %switch_1_8609.i ], [ %13, %if.end405.i ], [ %3, %LeafBlock57 ], [ %3, %if.end379.i ]
  %17 = phi i32 [ %4, %if.end421.i ], [ %4, %if.end225.i ], [ %4, %switch_1_8481.i ], [ %4, %switch_1_8466.i ], [ %4, %if.else234.i ], [ %11, %_L___0.i ], [ %4, %if.end328.i ], [ %4, %switch_1_8561.i ], [ %4, %if.then120.i ], [ %4, %if.else358.i ], [ %4, %if.end354.i ], [ %4, %switch_1_8593.i ], [ %4, %switch_1_8609.i ], [ %4, %if.end405.i ], [ %4, %LeafBlock57 ], [ %4, %if.end379.i ]
  %18 = phi i64 [ %5, %if.end421.i ], [ %5, %if.end225.i ], [ %5, %switch_1_8481.i ], [ %5, %switch_1_8466.i ], [ %5, %if.else234.i ], [ %conv246.i, %_L___0.i ], [ %5, %if.end328.i ], [ %5, %switch_1_8561.i ], [ %5, %if.then120.i ], [ %5, %if.else358.i ], [ %5, %if.end354.i ], [ %5, %switch_1_8593.i ], [ %5, %switch_1_8609.i ], [ %5, %if.end405.i ], [ %5, %LeafBlock57 ], [ %5, %if.end379.i ]
  %.928 = phi i32 [ 8640, %if.end421.i ], [ %.927, %if.end225.i ], [ 8482, %switch_1_8481.i ], [ %.927, %switch_1_8466.i ], [ %.927, %if.else234.i ], [ %.927, %_L___0.i ], [ 8576, %if.end328.i ], [ 8576, %switch_1_8561.i ], [ %.927, %if.then120.i ], [ %.927, %if.else358.i ], [ %.927, %if.end354.i ], [ %.927, %switch_1_8593.i ], [ %.927, %switch_1_8609.i ], [ %.927, %if.end405.i ], [ %.927, %LeafBlock57 ], [ %.927, %if.end379.i ]
  %19 = phi i32 [ 8448, %if.end421.i ], [ 8656, %if.end225.i ], [ 8448, %switch_1_8481.i ], [ 8496, %switch_1_8466.i ], [ 8528, %if.else234.i ], [ 8544, %_L___0.i ], [ 8448, %if.end328.i ], [ 8448, %switch_1_8561.i ], [ %.927, %if.then120.i ], [ 8592, %if.else358.i ], [ 8466, %if.end354.i ], [ 8608, %switch_1_8593.i ], [ 8640, %switch_1_8609.i ], [ 8672, %if.end405.i ], [ 3, %LeafBlock57 ], [ 3, %if.end379.i ]
  %call467.i = tail call i32 (...)* @nondet_int() nounwind
  %tobool468.i = icmp eq i32 %call467.i, 0
  br i1 %tobool468.i, label %if.then471.i, label %while.body.i

if.then471.i:                                     ; preds = %switch_1_break.i
  %cmp472.i = icmp eq i32 %6, 8528
  %cmp475.i = icmp eq i32 %19, 8544
  %or.cond34 = and i1 %cmp472.i, %cmp475.i
  br i1 %or.cond34, label %if.then477.i, label %if.then497.i

if.then477.i:                                     ; preds = %if.then471.i
  br label %NodeBlock145

NodeBlock145:                                     ; preds = %if.then477.i
  %Pivot146 = icmp slt i32 %17, 1024
  br i1 %Pivot146, label %LeafBlock141, label %LeafBlock143

LeafBlock143:                                     ; preds = %NodeBlock145
  %SwitchLeaf144 = icmp eq i32 %17, 1024
  br i1 %SwitchLeaf144, label %if.then497.i, label %NewDefault140

LeafBlock141:                                     ; preds = %NodeBlock145
  %SwitchLeaf142 = icmp eq i32 %17, 512
  br i1 %SwitchLeaf142, label %if.then497.i, label %NewDefault140

NewDefault140:                                    ; preds = %LeafBlock141, %LeafBlock143
  br label %if.then483.i

if.then483.i:                                     ; preds = %NewDefault140
  %cmp484.i = icmp eq i64 %15, 4294967294
  %cmp487.i = icmp eq i64 %18, 4294967266
  %or.cond35 = or i1 %cmp484.i, %cmp487.i
  br i1 %or.cond35, label %if.then497.i, label %ERROR.i

if.then497.i:                                     ; preds = %if.then483.i, %LeafBlock141, %LeafBlock143, %if.then471.i
  %call498.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp499.i = icmp slt i32 %call498.i, 1
  br i1 %cmp499.i, label %ssl3_accept.exit.loopexit, label %while.body.i

ERROR.i:                                          ; preds = %if.then483.i, %switch_1_8641.i, %_L___1.i
  ret i32 5

NewDefault125:                                    ; preds = %LeafBlock126, %LeafBlock128
  br label %ssl3_accept.exit.loopexit

ssl3_accept.exit.loopexit:                        ; preds = %NewDefault125, %if.then497.i, %switch_1_8673.i, %if.end405.i, %if.end379.i, %switch_1_8609.i, %switch_1_8593.i, %if.else358.i, %switch_1_8577.i, %switch_1_8561.i, %if.end328.i, %_L___0.i, %if.else234.i, %switch_1_8497.i, %switch_1_8466.i, %switch_1_8481.i, %LeafBlock, %LeafBlock44, %LeafBlock109, %LeafBlock111, %LeafBlock113
  br label %ssl3_accept.exit

ssl3_accept.exit:                                 ; preds = %ssl3_accept.exit.loopexit, %LeafBlock126, %entry
  br label %_UFO__exit

_UFO__exit:                                       ; preds = %_UFO__exit, %ssl3_accept.exit
  br label %_UFO__exit
}

declare i32 @nondet_1() readnone

declare i64 @nondet_2() readnone

declare i32 @exit(i32)
