; ModuleID = './tests/bench2/ssh-simplified/s3_srvr_1.cil.co.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [6 x i8] c"error\00", align 1

declare i32 @printf(...)

declare i32 @nondet_int(...)

define i32 @main() nounwind uwtable {
entry:
  %0 = tail call i32 @nondet_1() nounwind
  %cmp.i = icmp eq i32 %0, 0
  br i1 %cmp.i, label %ssl3_accept.exit, label %while.body.i.preheader

while.body.i.preheader:                           ; preds = %entry
  %1 = tail call i64 @nondet_2() nounwind
  %tobool181.i = icmp eq i32 %0, -256
  %cmp281.i = icmp slt i64 %1, 1
  br label %while.body.i

NewDefault114:                                    ; preds = %LeafBlock115, %LeafBlock117
  br label %while.body.i

while.body.i:                                     ; preds = %if.end350.i, %if.end336.i, %if.end319.i, %switch_1_8609.i, %switch_1_8593.i, %if.else294.i, %if.end290.i, %switch_1_8561.i, %_L___1.i, %_L___0.i, %if.else183.i, %switch_1_8513.i, %if.end176.i, %switch_1_8466.i, %switch_1_8481.i, %if.then95.i, %LeafBlock53, %NewDefault114, %while.body.i.preheader
  %2 = phi i32 [ 0, %while.body.i.preheader ], [ %7, %if.end336.i ], [ %6, %if.end319.i ], [ %5, %if.end176.i ], [ %2, %switch_1_8481.i ], [ %4, %switch_1_8466.i ], [ %2, %switch_1_8513.i ], [ %2, %if.else183.i ], [ %2, %_L___0.i ], [ %2, %_L___1.i ], [ %2, %switch_1_8561.i ], [ %2, %if.then95.i ], [ %2, %if.else294.i ], [ %2, %if.end290.i ], [ %2, %switch_1_8593.i ], [ %2, %switch_1_8609.i ], [ %2, %LeafBlock53 ], [ %2, %NewDefault114 ], [ %8, %if.end350.i ]
  %.926 = phi i32 [ %0, %while.body.i.preheader ], [ %.926, %if.end336.i ], [ %.926, %if.end319.i ], [ %.926, %if.end176.i ], [ 8482, %switch_1_8481.i ], [ %.926, %switch_1_8466.i ], [ %.926, %switch_1_8513.i ], [ %.926, %if.else183.i ], [ %.926, %_L___0.i ], [ 8576, %_L___1.i ], [ 8576, %switch_1_8561.i ], [ %.926, %if.then95.i ], [ %.926, %if.else294.i ], [ %.926, %if.end290.i ], [ %.926, %switch_1_8593.i ], [ %.926, %switch_1_8609.i ], [ %.926, %LeafBlock53 ], [ %.926, %NewDefault114 ], [ 8640, %if.end350.i ]
  %3 = phi i32 [ 8464, %while.body.i.preheader ], [ 8672, %if.end336.i ], [ 3, %if.end319.i ], [ 8656, %if.end176.i ], [ 8448, %switch_1_8481.i ], [ 8496, %switch_1_8466.i ], [ 8528, %switch_1_8513.i ], [ 8528, %if.else183.i ], [ 8544, %_L___0.i ], [ 8448, %_L___1.i ], [ 8448, %switch_1_8561.i ], [ %.926, %if.then95.i ], [ 8592, %if.else294.i ], [ 8466, %if.end290.i ], [ 8608, %switch_1_8593.i ], [ 8640, %switch_1_8609.i ], [ 3, %LeafBlock53 ], [ 8560, %NewDefault114 ], [ 8448, %if.end350.i ]
  br label %NodeBlock104

NodeBlock104:                                     ; preds = %while.body.i
  %Pivot105 = icmp slt i32 %3, 8528
  br i1 %Pivot105, label %NodeBlock67, label %NodeBlock102

NodeBlock102:                                     ; preds = %NodeBlock104
  %Pivot103 = icmp slt i32 %3, 8592
  br i1 %Pivot103, label %NodeBlock85, label %NodeBlock100

NodeBlock100:                                     ; preds = %NodeBlock102
  %Pivot101 = icmp slt i32 %3, 16384
  br i1 %Pivot101, label %NodeBlock92, label %NodeBlock98

NodeBlock98:                                      ; preds = %NodeBlock100
  %Pivot99 = icmp slt i32 %3, 24576
  br i1 %Pivot99, label %LeafBlock94, label %LeafBlock96

LeafBlock96:                                      ; preds = %NodeBlock98
  %SwitchLeaf97 = icmp eq i32 %3, 24576
  br i1 %SwitchLeaf97, label %ssl3_accept.exit.loopexit, label %NewDefault

LeafBlock94:                                      ; preds = %NodeBlock98
  %SwitchLeaf95 = icmp eq i32 %3, 16384
  br i1 %SwitchLeaf95, label %ssl3_accept.exit.loopexit, label %NewDefault

NodeBlock92:                                      ; preds = %NodeBlock100
  %Pivot93 = icmp slt i32 %3, 12292
  br i1 %Pivot93, label %LeafBlock87, label %LeafBlock90

LeafBlock90:                                      ; preds = %NodeBlock92
  %SwitchLeaf91 = icmp eq i32 %3, 12292
  br i1 %SwitchLeaf91, label %ssl3_accept.exit.loopexit, label %NewDefault

LeafBlock87:                                      ; preds = %NodeBlock92
  %.off88 = add i32 %3, -8592
  %SwitchLeaf89 = icmp ule i32 %.off88, 1
  br i1 %SwitchLeaf89, label %switch_1_8593.i, label %NewDefault

NodeBlock85:                                      ; preds = %NodeBlock102
  %Pivot86 = icmp slt i32 %3, 8560
  br i1 %Pivot86, label %NodeBlock75, label %NodeBlock83

NodeBlock83:                                      ; preds = %NodeBlock85
  %Pivot84 = icmp slt i32 %3, 8576
  br i1 %Pivot84, label %LeafBlock77, label %LeafBlock80

LeafBlock80:                                      ; preds = %NodeBlock83
  %.off81 = add i32 %3, -8576
  %SwitchLeaf82 = icmp ule i32 %.off81, 1
  br i1 %SwitchLeaf82, label %switch_1_8577.i, label %NewDefault

LeafBlock77:                                      ; preds = %NodeBlock83
  %.off78 = add i32 %3, -8560
  %SwitchLeaf79 = icmp ule i32 %.off78, 1
  br i1 %SwitchLeaf79, label %switch_1_8561.i, label %NewDefault

NodeBlock75:                                      ; preds = %NodeBlock85
  %Pivot76 = icmp slt i32 %3, 8544
  br i1 %Pivot76, label %LeafBlock69, label %LeafBlock72

LeafBlock72:                                      ; preds = %NodeBlock75
  %.off73 = add i32 %3, -8544
  %SwitchLeaf74 = icmp ule i32 %.off73, 1
  br i1 %SwitchLeaf74, label %switch_1_8545.i, label %NewDefault

LeafBlock69:                                      ; preds = %NodeBlock75
  %.off70 = add i32 %3, -8528
  %SwitchLeaf71 = icmp ule i32 %.off70, 1
  br i1 %SwitchLeaf71, label %_L___0.i, label %NewDefault

NodeBlock67:                                      ; preds = %NodeBlock104
  %Pivot68 = icmp slt i32 %3, 8480
  br i1 %Pivot68, label %NodeBlock48, label %NodeBlock65

NodeBlock65:                                      ; preds = %NodeBlock67
  %Pivot66 = icmp slt i32 %3, 8496
  br i1 %Pivot66, label %NodeBlock55, label %NodeBlock63

NodeBlock63:                                      ; preds = %NodeBlock65
  %Pivot64 = icmp slt i32 %3, 8512
  br i1 %Pivot64, label %LeafBlock57, label %LeafBlock60

LeafBlock60:                                      ; preds = %NodeBlock63
  %.off61 = add i32 %3, -8512
  %SwitchLeaf62 = icmp ule i32 %.off61, 1
  br i1 %SwitchLeaf62, label %switch_1_8513.i, label %NewDefault

LeafBlock57:                                      ; preds = %NodeBlock63
  %.off58 = add i32 %3, -8496
  %SwitchLeaf59 = icmp ule i32 %.off58, 1
  br i1 %SwitchLeaf59, label %switch_1_8497.i, label %NewDefault

NodeBlock55:                                      ; preds = %NodeBlock65
  %Pivot56 = icmp slt i32 %3, 8482
  br i1 %Pivot56, label %LeafBlock50, label %LeafBlock53

LeafBlock53:                                      ; preds = %NodeBlock55
  %SwitchLeaf54 = icmp eq i32 %3, 8482
  br i1 %SwitchLeaf54, label %while.body.i, label %NewDefault

LeafBlock50:                                      ; preds = %NodeBlock55
  %.off51 = add i32 %3, -8480
  %SwitchLeaf52 = icmp ule i32 %.off51, 1
  br i1 %SwitchLeaf52, label %switch_1_8481.i, label %NewDefault

NodeBlock48:                                      ; preds = %NodeBlock67
  %Pivot49 = icmp slt i32 %3, 8448
  br i1 %Pivot49, label %NodeBlock, label %NodeBlock46

NodeBlock46:                                      ; preds = %NodeBlock48
  %Pivot47 = icmp slt i32 %3, 8464
  br i1 %Pivot47, label %LeafBlock41, label %LeafBlock43

LeafBlock43:                                      ; preds = %NodeBlock46
  %.off44 = add i32 %3, -8464
  %SwitchLeaf45 = icmp ule i32 %.off44, 2
  br i1 %SwitchLeaf45, label %switch_1_8466.i, label %NewDefault

LeafBlock41:                                      ; preds = %NodeBlock46
  %SwitchLeaf42 = icmp eq i32 %3, 8448
  br i1 %SwitchLeaf42, label %if.then95.i, label %NewDefault

NodeBlock:                                        ; preds = %NodeBlock48
  %Pivot = icmp slt i32 %3, 8195
  br i1 %Pivot, label %LeafBlock, label %LeafBlock39

LeafBlock39:                                      ; preds = %NodeBlock
  %SwitchLeaf40 = icmp eq i32 %3, 8195
  br i1 %SwitchLeaf40, label %ssl3_accept.exit.loopexit, label %NewDefault

LeafBlock:                                        ; preds = %NodeBlock
  %SwitchLeaf = icmp eq i32 %3, 8192
  br i1 %SwitchLeaf, label %ssl3_accept.exit.loopexit, label %NewDefault

if.then95.i:                                      ; preds = %LeafBlock41
  %call276.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp278.i = icmp sgt i32 %call276.i, 0
  %or.cond31 = and i1 %cmp278.i, %cmp281.i
  br i1 %or.cond31, label %ssl3_accept.exit.loopexit, label %while.body.i

NewDefault:                                       ; preds = %LeafBlock, %LeafBlock39, %LeafBlock41, %LeafBlock43, %LeafBlock50, %LeafBlock53, %LeafBlock57, %LeafBlock60, %LeafBlock69, %LeafBlock72, %LeafBlock77, %LeafBlock80, %LeafBlock87, %LeafBlock90, %LeafBlock94, %LeafBlock96
  br label %if.else108.i

if.else108.i:                                     ; preds = %NewDefault
  %.off = add i32 %3, -8608
  %switch = icmp ult i32 %.off, 2
  br i1 %switch, label %switch_1_8609.i, label %if.else114.i

if.else114.i:                                     ; preds = %if.else108.i
  %.off33 = add i32 %3, -8640
  %switch34 = icmp ult i32 %.off33, 2
  br i1 %switch34, label %switch_1_8641.i, label %if.else120.i

if.else120.i:                                     ; preds = %if.else114.i
  %.off35 = add i32 %3, -8656
  %switch36 = icmp ult i32 %.off35, 2
  br i1 %switch36, label %switch_1_8657.i, label %if.else126.i

if.else126.i:                                     ; preds = %if.else120.i
  br label %NodeBlock112

NodeBlock112:                                     ; preds = %if.else126.i
  %Pivot113 = icmp slt i32 %3, 8672
  br i1 %Pivot113, label %LeafBlock107, label %LeafBlock109

LeafBlock109:                                     ; preds = %NodeBlock112
  %.off110 = add i32 %3, -8672
  %SwitchLeaf111 = icmp ule i32 %.off110, 1
  br i1 %SwitchLeaf111, label %switch_1_8673.i, label %NewDefault106

LeafBlock107:                                     ; preds = %NodeBlock112
  %SwitchLeaf108 = icmp eq i32 %3, 3
  br i1 %SwitchLeaf108, label %ssl3_accept.exit, label %NewDefault106

switch_1_8481.i:                                  ; preds = %LeafBlock50
  %call159.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp160.i = icmp slt i32 %call159.i, 1
  br i1 %cmp160.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8466.i:                                  ; preds = %LeafBlock43
  %call163.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp164.i = icmp eq i32 %2, 0
  %4 = select i1 %cmp164.i, i32 1, i32 %2
  %cmp167.i = icmp slt i32 %call163.i, 1
  br i1 %cmp167.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8497.i:                                  ; preds = %LeafBlock57
  %call170.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp174.i = icmp slt i32 %call170.i, 1
  br i1 %cmp174.i, label %ssl3_accept.exit.loopexit, label %if.end176.i

if.end176.i:                                      ; preds = %switch_1_8497.i
  %cmp171.i = icmp eq i32 %2, 1
  %5 = select i1 %cmp171.i, i32 2, i32 %2
  br label %while.body.i

switch_1_8513.i:                                  ; preds = %LeafBlock60
  br i1 %tobool181.i, label %if.else183.i, label %while.body.i

if.else183.i:                                     ; preds = %switch_1_8513.i
  %call184.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp185.i = icmp slt i32 %call184.i, 1
  br i1 %cmp185.i, label %ssl3_accept.exit.loopexit, label %while.body.i

_L___0.i:                                         ; preds = %LeafBlock69
  %call226.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp227.i = icmp slt i32 %call226.i, 1
  br i1 %cmp227.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8545.i:                                  ; preds = %LeafBlock72
  br label %NodeBlock119

NodeBlock119:                                     ; preds = %switch_1_8545.i
  %Pivot120 = icmp slt i32 %0, 0
  br i1 %Pivot120, label %LeafBlock115, label %LeafBlock117

LeafBlock117:                                     ; preds = %NodeBlock119
  %SwitchLeaf118 = icmp eq i32 %0, 0
  br i1 %SwitchLeaf118, label %_L___1.i, label %NewDefault114

LeafBlock115:                                     ; preds = %NodeBlock119
  %SwitchLeaf116 = icmp eq i32 %0, -4
  br i1 %SwitchLeaf116, label %_L___1.i, label %NewDefault114

_L___1.i:                                         ; preds = %LeafBlock115, %LeafBlock117
  %call262.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp263.i = icmp slt i32 %call262.i, 1
  br i1 %cmp263.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8561.i:                                  ; preds = %LeafBlock77
  %call271.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp272.i = icmp slt i32 %call271.i, 1
  br i1 %cmp272.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8577.i:                                  ; preds = %LeafBlock80
  %call286.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp287.i = icmp slt i32 %call286.i, 1
  br i1 %cmp287.i, label %ssl3_accept.exit.loopexit, label %if.end290.i

if.end290.i:                                      ; preds = %switch_1_8577.i
  %cmp291.i = icmp eq i32 %call286.i, 2
  br i1 %cmp291.i, label %while.body.i, label %if.else294.i

if.else294.i:                                     ; preds = %if.end290.i
  %call295.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp296.i = icmp slt i32 %call295.i, 1
  br i1 %cmp296.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8593.i:                                  ; preds = %LeafBlock87
  %call301.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp302.i = icmp slt i32 %call301.i, 1
  br i1 %cmp302.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8609.i:                                  ; preds = %if.else108.i
  %call306.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp307.i = icmp slt i32 %call306.i, 1
  br i1 %cmp307.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8641.i:                                  ; preds = %if.else114.i
  %call311.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp316.i = icmp slt i32 %call311.i, 1
  br i1 %cmp316.i, label %ssl3_accept.exit.loopexit, label %if.end319.i

if.end319.i:                                      ; preds = %switch_1_8641.i
  %cmp312.i = icmp eq i32 %2, 3
  %6 = select i1 %cmp312.i, i32 4, i32 %2
  br label %while.body.i

switch_1_8657.i:                                  ; preds = %if.else120.i
  %call324.i = tail call i32 (...)* @nondet_int() nounwind
  %tobool325.i = icmp eq i32 %call324.i, 0
  br i1 %tobool325.i, label %ssl3_accept.exit.loopexit, label %if.end327.i

if.end327.i:                                      ; preds = %switch_1_8657.i
  %call328.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp333.i = icmp slt i32 %call328.i, 1
  br i1 %cmp333.i, label %ssl3_accept.exit.loopexit, label %if.end336.i

if.end336.i:                                      ; preds = %if.end327.i
  %cmp329.i = icmp eq i32 %2, 2
  %7 = select i1 %cmp329.i, i32 3, i32 %2
  %call337.i = tail call i32 (...)* @nondet_int() nounwind
  %tobool338.i = icmp eq i32 %call337.i, 0
  br i1 %tobool338.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8673.i:                                  ; preds = %LeafBlock109
  %call341.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp342.i = icmp eq i32 %2, 4
  br i1 %cmp342.i, label %if.end350.i, label %if.else345.i

if.else345.i:                                     ; preds = %switch_1_8673.i
  %cmp346.i = icmp eq i32 %2, 5
  br i1 %cmp346.i, label %if.then348.i, label %if.end350.i

if.then348.i:                                     ; preds = %if.else345.i
  %call.i.i = tail call i32 (...)* @printf(i8* getelementptr inbounds ([6 x i8]* @.str, i64 0, i64 0)) nounwind
  br label %if.end350.i

if.end350.i:                                      ; preds = %if.then348.i, %if.else345.i, %switch_1_8673.i
  %8 = phi i32 [ %2, %if.else345.i ], [ 5, %if.then348.i ], [ 5, %switch_1_8673.i ]
  %cmp351.i = icmp slt i32 %call341.i, 1
  br i1 %cmp351.i, label %ssl3_accept.exit.loopexit, label %while.body.i

NewDefault106:                                    ; preds = %LeafBlock107, %LeafBlock109
  br label %ssl3_accept.exit.loopexit

ssl3_accept.exit.loopexit:                        ; preds = %NewDefault106, %if.end350.i, %if.end336.i, %if.end327.i, %switch_1_8657.i, %switch_1_8641.i, %switch_1_8609.i, %switch_1_8593.i, %if.else294.i, %switch_1_8577.i, %switch_1_8561.i, %_L___1.i, %_L___0.i, %if.else183.i, %switch_1_8497.i, %switch_1_8466.i, %switch_1_8481.i, %if.then95.i, %LeafBlock, %LeafBlock39, %LeafBlock90, %LeafBlock94, %LeafBlock96
  br label %ssl3_accept.exit

ssl3_accept.exit:                                 ; preds = %ssl3_accept.exit.loopexit, %LeafBlock107, %entry
  br label %_UFO__exit

_UFO__exit:                                       ; preds = %_UFO__exit, %ssl3_accept.exit
  br label %_UFO__exit
}

declare i32 @nondet_1() readnone

declare i64 @nondet_2() readnone
