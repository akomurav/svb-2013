; ModuleID = './tests/bench2/ssh-simplified/s3_srvr_14_BUG.cil.co.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

declare i32 @nondet_int(...)

define i32 @main() nounwind uwtable {
entry:
  %0 = tail call i32 @nondet_1() nounwind
  %call.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp.i = icmp eq i32 %0, 0
  br i1 %cmp.i, label %ssl3_accept.exit, label %while.body.i.preheader

while.body.i.preheader:                           ; preds = %entry
  %tobool233.i = icmp eq i32 %0, -256
  %cmp476.i = icmp eq i32 %0, -12288
  %cmp482.i = icmp eq i32 %0, -16384
  br label %while.body.i.outer

while.body.i.outer:                               ; preds = %if.then493.i, %while.body.i.preheader
  %.ph65 = phi i32 [ 0, %while.body.i.preheader ], [ %.ph41, %if.then493.i ]
  %.926.ph = phi i32 [ %0, %while.body.i.preheader ], [ %.927.ph47, %if.then493.i ]
  %.ph67 = phi i32 [ 8464, %while.body.i.preheader ], [ %.ph3149, %if.then493.i ]
  br i1 %tobool233.i, label %while.body.i.us, label %while.body.i

while.body.i.us:                                  ; preds = %if.else103.i.us, %while.body.i.outer
  %1 = phi i32 [ %.ph67, %while.body.i.outer ], [ 8560, %if.else103.i.us ]
  br label %NodeBlock158

NodeBlock158:                                     ; preds = %while.body.i.us
  %Pivot159 = icmp slt i32 %1, 8496
  br i1 %Pivot159, label %NodeBlock131, label %NodeBlock156

NodeBlock156:                                     ; preds = %NodeBlock158
  %Pivot157 = icmp slt i32 %1, 12292
  br i1 %Pivot157, label %NodeBlock144, label %NodeBlock154

NodeBlock154:                                     ; preds = %NodeBlock156
  %Pivot155 = icmp slt i32 %1, 16384
  br i1 %Pivot155, label %LeafBlock146, label %NodeBlock152

NodeBlock152:                                     ; preds = %NodeBlock154
  %Pivot153 = icmp slt i32 %1, 24576
  br i1 %Pivot153, label %LeafBlock148, label %LeafBlock150

LeafBlock150:                                     ; preds = %NodeBlock152
  %SwitchLeaf151 = icmp eq i32 %1, 24576
  br i1 %SwitchLeaf151, label %ssl3_accept.exit.loopexit, label %NewDefault

LeafBlock148:                                     ; preds = %NodeBlock152
  %SwitchLeaf149 = icmp eq i32 %1, 16384
  br i1 %SwitchLeaf149, label %ssl3_accept.exit.loopexit, label %NewDefault

LeafBlock146:                                     ; preds = %NodeBlock154
  %SwitchLeaf147 = icmp eq i32 %1, 12292
  br i1 %SwitchLeaf147, label %ssl3_accept.exit.loopexit, label %NewDefault

NodeBlock144:                                     ; preds = %NodeBlock156
  %Pivot145 = icmp slt i32 %1, 8512
  br i1 %Pivot145, label %LeafBlock133, label %NodeBlock142

NodeBlock142:                                     ; preds = %NodeBlock144
  %Pivot143 = icmp slt i32 %1, 8528
  br i1 %Pivot143, label %LeafBlock136, label %LeafBlock139

LeafBlock139:                                     ; preds = %NodeBlock142
  %.off140 = add i32 %1, -8528
  %SwitchLeaf141 = icmp ule i32 %.off140, 1
  br i1 %SwitchLeaf141, label %_L___0.i, label %NewDefault

LeafBlock136:                                     ; preds = %NodeBlock142
  %.off137 = add i32 %1, -8512
  %SwitchLeaf138 = icmp ule i32 %.off137, 1
  br i1 %SwitchLeaf138, label %if.else235.i, label %NewDefault

LeafBlock133:                                     ; preds = %NodeBlock144
  %.off134 = add i32 %1, -8496
  %SwitchLeaf135 = icmp ule i32 %.off134, 1
  br i1 %SwitchLeaf135, label %switch_1_8497.i, label %NewDefault

NodeBlock131:                                     ; preds = %NodeBlock158
  %Pivot132 = icmp slt i32 %1, 8464
  br i1 %Pivot132, label %NodeBlock, label %NodeBlock129

NodeBlock129:                                     ; preds = %NodeBlock131
  %Pivot130 = icmp slt i32 %1, 8480
  br i1 %Pivot130, label %LeafBlock119, label %NodeBlock127

NodeBlock127:                                     ; preds = %NodeBlock129
  %Pivot128 = icmp slt i32 %1, 8482
  br i1 %Pivot128, label %LeafBlock122, label %LeafBlock125

LeafBlock125:                                     ; preds = %NodeBlock127
  %SwitchLeaf126 = icmp eq i32 %1, 8482
  br i1 %SwitchLeaf126, label %if.then475.i, label %NewDefault

LeafBlock122:                                     ; preds = %NodeBlock127
  %.off123 = add i32 %1, -8480
  %SwitchLeaf124 = icmp ule i32 %.off123, 1
  br i1 %SwitchLeaf124, label %switch_1_8481.i, label %NewDefault

LeafBlock119:                                     ; preds = %NodeBlock129
  %.off120 = add i32 %1, -8464
  %SwitchLeaf121 = icmp ule i32 %.off120, 2
  br i1 %SwitchLeaf121, label %switch_1_8466.i, label %NewDefault

NodeBlock:                                        ; preds = %NodeBlock131
  %Pivot = icmp slt i32 %1, 8195
  br i1 %Pivot, label %LeafBlock, label %LeafBlock117

LeafBlock117:                                     ; preds = %NodeBlock
  %SwitchLeaf118 = icmp eq i32 %1, 8195
  br i1 %SwitchLeaf118, label %ssl3_accept.exit.loopexit, label %NewDefault

LeafBlock:                                        ; preds = %NodeBlock
  %SwitchLeaf = icmp eq i32 %1, 8192
  br i1 %SwitchLeaf, label %ssl3_accept.exit.loopexit, label %NewDefault

NewDefault:                                       ; preds = %LeafBlock, %LeafBlock117, %LeafBlock119, %LeafBlock122, %LeafBlock125, %LeafBlock133, %LeafBlock136, %LeafBlock139, %LeafBlock146, %LeafBlock148, %LeafBlock150
  br label %if.else103.i.us

if.else103.i.us:                                  ; preds = %NewDefault
  %.off.us = add i32 %1, -8544
  %switch.us = icmp ult i32 %.off.us, 2
  br i1 %switch.us, label %while.body.i.us, label %if.else111.i

NewDefault249:                                    ; preds = %LeafBlock250, %LeafBlock252
  br label %while.body.i

while.body.i:                                     ; preds = %LeafBlock184, %NewDefault249, %while.body.i.outer
  %2 = phi i32 [ %.ph67, %while.body.i.outer ], [ 8560, %NewDefault249 ], [ 8528, %LeafBlock184 ]
  br label %NodeBlock206

NodeBlock206:                                     ; preds = %while.body.i
  %Pivot207 = icmp slt i32 %2, 8496
  br i1 %Pivot207, label %NodeBlock179, label %NodeBlock204

NodeBlock204:                                     ; preds = %NodeBlock206
  %Pivot205 = icmp slt i32 %2, 12292
  br i1 %Pivot205, label %NodeBlock192, label %NodeBlock202

NodeBlock202:                                     ; preds = %NodeBlock204
  %Pivot203 = icmp slt i32 %2, 16384
  br i1 %Pivot203, label %LeafBlock194, label %NodeBlock200

NodeBlock200:                                     ; preds = %NodeBlock202
  %Pivot201 = icmp slt i32 %2, 24576
  br i1 %Pivot201, label %LeafBlock196, label %LeafBlock198

LeafBlock198:                                     ; preds = %NodeBlock200
  %SwitchLeaf199 = icmp eq i32 %2, 24576
  br i1 %SwitchLeaf199, label %ssl3_accept.exit.loopexit, label %NewDefault160

LeafBlock196:                                     ; preds = %NodeBlock200
  %SwitchLeaf197 = icmp eq i32 %2, 16384
  br i1 %SwitchLeaf197, label %ssl3_accept.exit.loopexit, label %NewDefault160

LeafBlock194:                                     ; preds = %NodeBlock202
  %SwitchLeaf195 = icmp eq i32 %2, 12292
  br i1 %SwitchLeaf195, label %ssl3_accept.exit.loopexit, label %NewDefault160

NodeBlock192:                                     ; preds = %NodeBlock204
  %Pivot193 = icmp slt i32 %2, 8512
  br i1 %Pivot193, label %LeafBlock181, label %NodeBlock190

NodeBlock190:                                     ; preds = %NodeBlock192
  %Pivot191 = icmp slt i32 %2, 8528
  br i1 %Pivot191, label %LeafBlock184, label %LeafBlock187

LeafBlock187:                                     ; preds = %NodeBlock190
  %.off188 = add i32 %2, -8528
  %SwitchLeaf189 = icmp ule i32 %.off188, 1
  br i1 %SwitchLeaf189, label %_L___0.i, label %NewDefault160

LeafBlock184:                                     ; preds = %NodeBlock190
  %.off185 = add i32 %2, -8512
  %SwitchLeaf186 = icmp ule i32 %.off185, 1
  br i1 %SwitchLeaf186, label %while.body.i, label %NewDefault160

LeafBlock181:                                     ; preds = %NodeBlock192
  %.off182 = add i32 %2, -8496
  %SwitchLeaf183 = icmp ule i32 %.off182, 1
  br i1 %SwitchLeaf183, label %switch_1_8497.i, label %NewDefault160

NodeBlock179:                                     ; preds = %NodeBlock206
  %Pivot180 = icmp slt i32 %2, 8464
  br i1 %Pivot180, label %NodeBlock165, label %NodeBlock177

NodeBlock177:                                     ; preds = %NodeBlock179
  %Pivot178 = icmp slt i32 %2, 8480
  br i1 %Pivot178, label %LeafBlock167, label %NodeBlock175

NodeBlock175:                                     ; preds = %NodeBlock177
  %Pivot176 = icmp slt i32 %2, 8482
  br i1 %Pivot176, label %LeafBlock170, label %LeafBlock173

LeafBlock173:                                     ; preds = %NodeBlock175
  %SwitchLeaf174 = icmp eq i32 %2, 8482
  br i1 %SwitchLeaf174, label %if.then475.i, label %NewDefault160

LeafBlock170:                                     ; preds = %NodeBlock175
  %.off171 = add i32 %2, -8480
  %SwitchLeaf172 = icmp ule i32 %.off171, 1
  br i1 %SwitchLeaf172, label %switch_1_8481.i, label %NewDefault160

LeafBlock167:                                     ; preds = %NodeBlock177
  %.off168 = add i32 %2, -8464
  %SwitchLeaf169 = icmp ule i32 %.off168, 2
  br i1 %SwitchLeaf169, label %switch_1_8466.i, label %NewDefault160

NodeBlock165:                                     ; preds = %NodeBlock179
  %Pivot166 = icmp slt i32 %2, 8195
  br i1 %Pivot166, label %LeafBlock161, label %LeafBlock163

LeafBlock163:                                     ; preds = %NodeBlock165
  %SwitchLeaf164 = icmp eq i32 %2, 8195
  br i1 %SwitchLeaf164, label %ssl3_accept.exit.loopexit, label %NewDefault160

LeafBlock161:                                     ; preds = %NodeBlock165
  %SwitchLeaf162 = icmp eq i32 %2, 8192
  br i1 %SwitchLeaf162, label %ssl3_accept.exit.loopexit, label %NewDefault160

NewDefault160:                                    ; preds = %LeafBlock161, %LeafBlock163, %LeafBlock167, %LeafBlock170, %LeafBlock173, %LeafBlock181, %LeafBlock184, %LeafBlock187, %LeafBlock194, %LeafBlock196, %LeafBlock198
  br label %if.else103.i

if.else103.i:                                     ; preds = %NewDefault160
  %.off = add i32 %2, -8544
  %switch = icmp ult i32 %.off, 2
  br i1 %switch, label %switch_1_8545.i, label %if.else111.i

if.else111.i:                                     ; preds = %if.else103.i, %if.else103.i.us
  %.lcssa74 = phi i32 [ %1, %if.else103.i.us ], [ %2, %if.else103.i ]
  br label %NodeBlock247

NodeBlock247:                                     ; preds = %if.else111.i
  %Pivot248 = icmp slt i32 %.lcssa74, 8592
  br i1 %Pivot248, label %NodeBlock222, label %NodeBlock245

NodeBlock245:                                     ; preds = %NodeBlock247
  %Pivot246 = icmp slt i32 %.lcssa74, 8640
  br i1 %Pivot246, label %NodeBlock230, label %NodeBlock243

NodeBlock243:                                     ; preds = %NodeBlock245
  %Pivot244 = icmp slt i32 %.lcssa74, 8656
  br i1 %Pivot244, label %LeafBlock232, label %NodeBlock241

NodeBlock241:                                     ; preds = %NodeBlock243
  %Pivot242 = icmp slt i32 %.lcssa74, 8672
  br i1 %Pivot242, label %LeafBlock235, label %LeafBlock238

LeafBlock238:                                     ; preds = %NodeBlock241
  %.lcssa74.off239 = add i32 %.lcssa74, -8672
  %SwitchLeaf240 = icmp ule i32 %.lcssa74.off239, 1
  br i1 %SwitchLeaf240, label %switch_1_8673.i, label %NewDefault208

LeafBlock235:                                     ; preds = %NodeBlock241
  %.lcssa74.off236 = add i32 %.lcssa74, -8656
  %SwitchLeaf237 = icmp ule i32 %.lcssa74.off236, 1
  br i1 %SwitchLeaf237, label %if.end385.i, label %NewDefault208

LeafBlock232:                                     ; preds = %NodeBlock243
  %.lcssa74.off233 = add i32 %.lcssa74, -8640
  %SwitchLeaf234 = icmp ule i32 %.lcssa74.off233, 1
  br i1 %SwitchLeaf234, label %switch_1_8641.i, label %NewDefault208

NodeBlock230:                                     ; preds = %NodeBlock245
  %Pivot231 = icmp slt i32 %.lcssa74, 8608
  br i1 %Pivot231, label %LeafBlock224, label %LeafBlock227

LeafBlock227:                                     ; preds = %NodeBlock230
  %.lcssa74.off228 = add i32 %.lcssa74, -8608
  %SwitchLeaf229 = icmp ule i32 %.lcssa74.off228, 1
  br i1 %SwitchLeaf229, label %switch_1_8609.i, label %NewDefault208

LeafBlock224:                                     ; preds = %NodeBlock230
  %.lcssa74.off225 = add i32 %.lcssa74, -8592
  %SwitchLeaf226 = icmp ule i32 %.lcssa74.off225, 1
  br i1 %SwitchLeaf226, label %switch_1_8593.i, label %NewDefault208

NodeBlock222:                                     ; preds = %NodeBlock247
  %Pivot223 = icmp slt i32 %.lcssa74, 8560
  br i1 %Pivot223, label %NodeBlock213, label %NodeBlock220

NodeBlock220:                                     ; preds = %NodeBlock222
  %Pivot221 = icmp slt i32 %.lcssa74, 8576
  br i1 %Pivot221, label %LeafBlock215, label %LeafBlock217

LeafBlock217:                                     ; preds = %NodeBlock220
  %.lcssa74.off218 = add i32 %.lcssa74, -8576
  %SwitchLeaf219 = icmp ule i32 %.lcssa74.off218, 1
  br i1 %SwitchLeaf219, label %switch_1_8577.i, label %NewDefault208

LeafBlock215:                                     ; preds = %NodeBlock220
  %.lcssa74.off = add i32 %.lcssa74, -8560
  %SwitchLeaf216 = icmp ule i32 %.lcssa74.off, 1
  br i1 %SwitchLeaf216, label %switch_1_8561.i, label %NewDefault208

NodeBlock213:                                     ; preds = %NodeBlock222
  %Pivot214 = icmp slt i32 %.lcssa74, 8448
  br i1 %Pivot214, label %LeafBlock209, label %LeafBlock211

LeafBlock211:                                     ; preds = %NodeBlock213
  %SwitchLeaf212 = icmp eq i32 %.lcssa74, 8448
  br i1 %SwitchLeaf212, label %if.then475.i, label %NewDefault208

LeafBlock209:                                     ; preds = %NodeBlock213
  %SwitchLeaf210 = icmp eq i32 %.lcssa74, 3
  br i1 %SwitchLeaf210, label %ssl3_accept.exit, label %NewDefault208

switch_1_8481.i:                                  ; preds = %LeafBlock170, %LeafBlock122
  %.lcssa68 = phi i32 [ %1, %LeafBlock122 ], [ %2, %LeafBlock170 ]
  %call205.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp206.i = icmp slt i32 %call205.i, 1
  br i1 %cmp206.i, label %ssl3_accept.exit.loopexit, label %if.then475.i

switch_1_8466.i:                                  ; preds = %LeafBlock167, %LeafBlock119
  %.lcssa70 = phi i32 [ %1, %LeafBlock119 ], [ %2, %LeafBlock167 ]
  %call210.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp211.i = icmp eq i32 %.ph65, 0
  %3 = select i1 %cmp211.i, i32 1, i32 %.ph65
  %cmp215.i = icmp slt i32 %call210.i, 1
  br i1 %cmp215.i, label %ssl3_accept.exit.loopexit, label %if.then475.i

switch_1_8497.i:                                  ; preds = %LeafBlock181, %LeafBlock133
  %.lcssa71 = phi i32 [ %1, %LeafBlock133 ], [ %2, %LeafBlock181 ]
  %call219.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp224.i = icmp slt i32 %call219.i, 1
  br i1 %cmp224.i, label %ssl3_accept.exit.loopexit, label %if.end227.i

if.end227.i:                                      ; preds = %switch_1_8497.i
  %cmp220.i = icmp eq i32 %.ph65, 1
  %4 = select i1 %cmp220.i, i32 2, i32 %.ph65
  br label %if.then475.i

if.else235.i:                                     ; preds = %LeafBlock136
  %call236.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp237.i = icmp slt i32 %call236.i, 1
  br i1 %cmp237.i, label %ssl3_accept.exit.loopexit, label %if.then484.i

_L___0.i:                                         ; preds = %LeafBlock187, %LeafBlock139
  %.lcssa72 = phi i32 [ %1, %LeafBlock139 ], [ %2, %LeafBlock187 ]
  %call277.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp278.i = icmp slt i32 %call277.i, 1
  br i1 %cmp278.i, label %ssl3_accept.exit.loopexit, label %if.then475.i

switch_1_8545.i:                                  ; preds = %if.else103.i
  br label %NodeBlock254

NodeBlock254:                                     ; preds = %switch_1_8545.i
  %Pivot255 = icmp slt i32 %0, 0
  br i1 %Pivot255, label %LeafBlock250, label %LeafBlock252

LeafBlock252:                                     ; preds = %NodeBlock254
  %SwitchLeaf253 = icmp eq i32 %0, 0
  br i1 %SwitchLeaf253, label %_L___1.i, label %NewDefault249

LeafBlock250:                                     ; preds = %NodeBlock254
  %SwitchLeaf251 = icmp eq i32 %0, -4
  br i1 %SwitchLeaf251, label %_L___1.i, label %NewDefault249

_L___1.i:                                         ; preds = %LeafBlock250, %LeafBlock252
  %call313.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp314.i = icmp slt i32 %call313.i, 1
  br i1 %cmp314.i, label %ssl3_accept.exit.loopexit, label %if.then475.i

switch_1_8561.i:                                  ; preds = %LeafBlock215
  %call322.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp323.i = icmp slt i32 %call322.i, 1
  br i1 %cmp323.i, label %ssl3_accept.exit.loopexit, label %if.then475.i

switch_1_8577.i:                                  ; preds = %LeafBlock217
  %call335.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp336.i = icmp slt i32 %call335.i, 1
  br i1 %cmp336.i, label %ssl3_accept.exit.loopexit, label %if.end339.i

if.end339.i:                                      ; preds = %switch_1_8577.i
  %cmp340.i = icmp eq i32 %call335.i, 2
  br i1 %cmp340.i, label %if.then475.i, label %if.else343.i

if.else343.i:                                     ; preds = %if.end339.i
  %call344.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp345.i = icmp slt i32 %call344.i, 1
  br i1 %cmp345.i, label %ssl3_accept.exit.loopexit, label %if.then475.i

switch_1_8593.i:                                  ; preds = %LeafBlock224
  %call350.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp351.i = icmp slt i32 %call350.i, 1
  br i1 %cmp351.i, label %ssl3_accept.exit.loopexit, label %if.then475.i

switch_1_8609.i:                                  ; preds = %LeafBlock227
  %call355.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp356.i = icmp slt i32 %call355.i, 1
  br i1 %cmp356.i, label %ssl3_accept.exit.loopexit, label %if.then475.i

switch_1_8641.i:                                  ; preds = %LeafBlock232
  %call360.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp361.i = icmp eq i32 %.ph65, 4
  br i1 %cmp361.i, label %if.end374.i, label %if.else364.i

if.else364.i:                                     ; preds = %switch_1_8641.i
  %cmp365.i = icmp eq i32 %.ph65, 7
  br i1 %cmp365.i, label %if.end374.i, label %if.else368.i

if.else368.i:                                     ; preds = %if.else364.i
  %cmp369.i = icmp eq i32 %.ph65, 10
  br i1 %cmp369.i, label %ERROR.i, label %if.end374.i

if.end374.i:                                      ; preds = %if.else368.i, %if.else364.i, %switch_1_8641.i
  %5 = phi i32 [ %.ph65, %if.else368.i ], [ 5, %switch_1_8641.i ], [ 8, %if.else364.i ]
  %cmp375.i = icmp slt i32 %call360.i, 1
  br i1 %cmp375.i, label %ssl3_accept.exit.loopexit, label %if.then475.i

if.end385.i:                                      ; preds = %LeafBlock235
  %call386.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp387.i = icmp eq i32 %.ph65, 2
  br i1 %cmp387.i, label %if.end400.i, label %if.else390.i

if.else390.i:                                     ; preds = %if.end385.i
  %cmp391.i = icmp eq i32 %.ph65, 5
  br i1 %cmp391.i, label %if.end400.i, label %if.else394.i

if.else394.i:                                     ; preds = %if.else390.i
  %cmp395.i = icmp eq i32 %.ph65, 8
  br i1 %cmp395.i, label %if.then397.i, label %if.end400.i

if.then397.i:                                     ; preds = %if.else394.i
  br label %if.end400.i

if.end400.i:                                      ; preds = %if.then397.i, %if.else394.i, %if.else390.i, %if.end385.i
  %6 = phi i32 [ 9, %if.then397.i ], [ %.ph65, %if.else394.i ], [ 3, %if.end385.i ], [ 6, %if.else390.i ]
  %cmp401.i = icmp slt i32 %call386.i, 1
  br i1 %cmp401.i, label %ssl3_accept.exit.loopexit, label %if.then475.i

switch_1_8673.i:                                  ; preds = %LeafBlock238
  %call408.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp409.i = icmp eq i32 %.ph65, 3
  br i1 %cmp409.i, label %if.end422.i, label %if.else412.i

if.else412.i:                                     ; preds = %switch_1_8673.i
  %cmp413.i = icmp eq i32 %.ph65, 6
  br i1 %cmp413.i, label %if.end422.i, label %if.else416.i

if.else416.i:                                     ; preds = %if.else412.i
  %cmp417.i = icmp eq i32 %.ph65, 9
  br i1 %cmp417.i, label %if.then419.i, label %if.end422.i

if.then419.i:                                     ; preds = %if.else416.i
  br label %if.end422.i

if.end422.i:                                      ; preds = %if.then419.i, %if.else416.i, %if.else412.i, %switch_1_8673.i
  %7 = phi i32 [ 10, %if.then419.i ], [ %.ph65, %if.else416.i ], [ 4, %switch_1_8673.i ], [ 7, %if.else412.i ]
  %cmp423.i = icmp slt i32 %call408.i, 1
  br i1 %cmp423.i, label %ssl3_accept.exit.loopexit, label %if.then475.i

if.then475.i:                                     ; preds = %if.end422.i, %if.end400.i, %if.end374.i, %switch_1_8609.i, %switch_1_8593.i, %if.else343.i, %if.end339.i, %switch_1_8561.i, %_L___1.i, %_L___0.i, %if.end227.i, %switch_1_8466.i, %switch_1_8481.i, %LeafBlock211, %LeafBlock173, %LeafBlock125
  %8 = phi i32 [ %.lcssa71, %if.end227.i ], [ %.lcssa68, %switch_1_8481.i ], [ %.lcssa70, %switch_1_8466.i ], [ %.lcssa72, %_L___0.i ], [ %2, %_L___1.i ], [ %.lcssa74, %switch_1_8561.i ], [ %.lcssa74, %if.else343.i ], [ %.lcssa74, %if.end339.i ], [ %.lcssa74, %switch_1_8593.i ], [ %.lcssa74, %switch_1_8609.i ], [ %.lcssa74, %if.end400.i ], [ 8448, %LeafBlock211 ], [ %.lcssa74, %if.end374.i ], [ %.lcssa74, %if.end422.i ], [ 8482, %LeafBlock173 ], [ 8482, %LeafBlock125 ]
  %.ph = phi i32 [ %4, %if.end227.i ], [ %.ph65, %switch_1_8481.i ], [ %3, %switch_1_8466.i ], [ %.ph65, %_L___0.i ], [ %.ph65, %_L___1.i ], [ %.ph65, %switch_1_8561.i ], [ %.ph65, %if.else343.i ], [ %.ph65, %if.end339.i ], [ %.ph65, %switch_1_8593.i ], [ %.ph65, %switch_1_8609.i ], [ %6, %if.end400.i ], [ %.ph65, %LeafBlock211 ], [ %5, %if.end374.i ], [ %7, %if.end422.i ], [ %.ph65, %LeafBlock173 ], [ %.ph65, %LeafBlock125 ]
  %.927.ph = phi i32 [ %.926.ph, %if.end227.i ], [ 8482, %switch_1_8481.i ], [ %.926.ph, %switch_1_8466.i ], [ %.926.ph, %_L___0.i ], [ 8576, %_L___1.i ], [ 8576, %switch_1_8561.i ], [ %.926.ph, %if.else343.i ], [ %.926.ph, %if.end339.i ], [ %.926.ph, %switch_1_8593.i ], [ %.926.ph, %switch_1_8609.i ], [ %.926.ph, %if.end400.i ], [ %.926.ph, %LeafBlock211 ], [ %.926.ph, %if.end374.i ], [ 8640, %if.end422.i ], [ %.926.ph, %LeafBlock173 ], [ %.926.ph, %LeafBlock125 ]
  %.ph31 = phi i32 [ 8656, %if.end227.i ], [ 8448, %switch_1_8481.i ], [ 8496, %switch_1_8466.i ], [ 8544, %_L___0.i ], [ 8448, %_L___1.i ], [ 8448, %switch_1_8561.i ], [ 8592, %if.else343.i ], [ 8466, %if.end339.i ], [ 8608, %switch_1_8593.i ], [ 8640, %switch_1_8609.i ], [ 8672, %if.end400.i ], [ %.926.ph, %LeafBlock211 ], [ 3, %if.end374.i ], [ 8448, %if.end422.i ], [ 3, %LeafBlock173 ], [ 3, %LeafBlock125 ]
  br i1 %cmp476.i, label %if.then493.i, label %if.then478.i

if.then478.i:                                     ; preds = %if.then475.i
  %cmp479.i = icmp eq i32 %.ph31, 8656
  %brmerge = or i1 %cmp479.i, %cmp482.i
  br i1 %brmerge, label %if.then493.i, label %if.then484.i

if.then484.i:                                     ; preds = %if.then478.i, %if.else235.i
  %9 = phi i32 [ %1, %if.else235.i ], [ %8, %if.then478.i ]
  %.ph31485161 = phi i32 [ 8528, %if.else235.i ], [ %.ph31, %if.then478.i ]
  %.927.ph465260 = phi i32 [ %.926.ph, %if.else235.i ], [ %.927.ph, %if.then478.i ]
  %.ph405557 = phi i32 [ %.ph65, %if.else235.i ], [ %.ph, %if.then478.i ]
  %cmp485.i = icmp eq i32 %9, 8496
  br i1 %cmp485.i, label %ERROR.i, label %if.then493.i

if.then493.i:                                     ; preds = %if.then484.i, %if.then478.i, %if.then475.i
  %.ph3149 = phi i32 [ %.ph31, %if.then478.i ], [ %.ph31485161, %if.then484.i ], [ %.ph31, %if.then475.i ]
  %.927.ph47 = phi i32 [ %.927.ph, %if.then478.i ], [ %.927.ph465260, %if.then484.i ], [ %.927.ph, %if.then475.i ]
  %.ph41 = phi i32 [ %.ph, %if.then478.i ], [ %.ph405557, %if.then484.i ], [ %.ph, %if.then475.i ]
  %call494.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp495.i = icmp slt i32 %call494.i, 1
  br i1 %cmp495.i, label %ssl3_accept.exit.loopexit, label %while.body.i.outer

ERROR.i:                                          ; preds = %if.then484.i, %if.else368.i
  ret i32 5

NewDefault208:                                    ; preds = %LeafBlock209, %LeafBlock211, %LeafBlock215, %LeafBlock217, %LeafBlock224, %LeafBlock227, %LeafBlock232, %LeafBlock235, %LeafBlock238
  br label %ssl3_accept.exit.loopexit

ssl3_accept.exit.loopexit:                        ; preds = %NewDefault208, %if.then493.i, %if.end422.i, %if.end400.i, %if.end374.i, %switch_1_8609.i, %switch_1_8593.i, %if.else343.i, %switch_1_8577.i, %switch_1_8561.i, %_L___1.i, %_L___0.i, %if.else235.i, %switch_1_8497.i, %switch_1_8466.i, %switch_1_8481.i, %LeafBlock161, %LeafBlock163, %LeafBlock194, %LeafBlock196, %LeafBlock198, %LeafBlock, %LeafBlock117, %LeafBlock146, %LeafBlock148, %LeafBlock150
  br label %ssl3_accept.exit

ssl3_accept.exit:                                 ; preds = %ssl3_accept.exit.loopexit, %LeafBlock209, %entry
  br label %_UFO__exit

_UFO__exit:                                       ; preds = %_UFO__exit, %ssl3_accept.exit
  br label %_UFO__exit
}

declare i32 @nondet_1() readnone

declare i32 @exit(i32)
