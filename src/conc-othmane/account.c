/*-----Translation of program:Name-----*/

//-- Boolean non deterministic function definition.
_Bool nondet_bool(void);
int nondet_int(void);

//-- Assume function.
void assume(_Bool bVal) {
  if (!bVal) {
    while (1) {}
  } else {
    return;
  }
}

//-- Global Variables
//--  Instrumentation variables.
//--    Current "running" thread.
int running = 0;
//--    "progress": Force exploration progress.
_Bool progress = 1;
//--    Program counters.
int pc_1 = 1;
int pc_2 = 1;
int pc_3 = 1;
int pc_4 = 1;
//--  Thread start global variables.
//--  Thread deposit global variables.
//--  Thread withdraw global variables.
//--  Thread check_result global variables.
//--  Shared Variables.
int x;
int y;
int z;
int balance;
_Bool init = 0;
_Bool deposit_done = 0;
_Bool withdraw_done = 0;

//-- Another instrumentation variable: bNonDet used for non determinstic vars.
_Bool bNonDet;



void main()
{
  
  //-- Main Loop .. stops when no progress is made
  while ((progress==1)) {
    
    //-- Reset progress to none (false):
    progress = 0;
    
    
    //-- Context switch block:
    bNonDet = nondet_bool();
    if (!(pc_1 == 6) && (bNonDet==1)) {
      running = 1;
    }
    bNonDet = nondet_bool();
    if (!(pc_2 == 3) && (bNonDet==1)) {
      running = 2;
    }
    bNonDet = nondet_bool();
    if (!(pc_3 == 3) && (bNonDet==1)) {
      running = 3;
    }
    bNonDet = nondet_bool();
    if (!(pc_4 == 3) && (bNonDet==1)) {
      running = 4;
    }
    
    //-- The proper thread statements:
    
    //-- Thread number 1 statements:
    if (!(pc_1 == 6) && (running == 1)) {
      bNonDet = nondet_bool();
      if ((pc_1 == 1) && (bNonDet==1)) {
        x = nondet_int();
        pc_1 = 2;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_1 == 2) && (bNonDet==1)) {
        y = nondet_int();
        pc_1 = 3;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_1 == 3) && (bNonDet==1)) {
        z = nondet_int();
        pc_1 = 4;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_1 == 4) && (bNonDet==1)) {
        balance = x;
        pc_1 = 5;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_1 == 5) && (bNonDet==1)) {
        init = 1;
        pc_1 = 6;
        progress = 1;
      }
    }
    
    //-- Thread number 2 statements:
    if (!(pc_2 == 3) && (running == 2)) {
      bNonDet = nondet_bool();
      if ((pc_2 == 1) && (bNonDet==1)) {
        assume((init==1));
        pc_2 = 2;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_2 == 2) && (bNonDet==1)) {
        balance = balance + y;
        deposit_done = 1;
        pc_2 = 3;
        progress = 1;
      }
    }
    
    //-- Thread number 3 statements:
    if (!(pc_3 == 3) && (running == 3)) {
      bNonDet = nondet_bool();
      if ((pc_3 == 1) && (bNonDet==1)) {
        assume((init==1));
        pc_3 = 2;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_3 == 2) && (bNonDet==1)) {
        balance = balance - z;
        withdraw_done = 1;
        pc_3 = 3;
        progress = 1;
      }
    }
    
    //-- Thread number 4 statements:
    if (!(pc_4 == 3) && (running == 4)) {
      bNonDet = nondet_bool();
      if ((pc_4 == 1) && (bNonDet==1)) {
        assume((init==1));
        pc_4 = 2;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_4 == 2) && (bNonDet==1)) {
        if ((deposit_done==1) && (withdraw_done==1)) {
          if (balance != ((x + y) - z)) goto EXIT;
        }
        pc_4 = 3;
        progress = 1;
      }
    }
    
  }

  while (1) {}
EXIT: return;
}
