/*-----Translation of program:Name-----*/

//-- Boolean non deterministic function definition.
_Bool nondet_bool(void);
int nondet_int(void);

//-- Assume function.
void assume(_Bool bVal) {
  if (!bVal) {
    while(1) {}
  } else {
    return;
  }
}

//-- Global Variables
//--  Instrumentation variables.
//--    Current "running" thread.
int running = 0;
//--    "progress": Force exploration progress.
_Bool progress = 1;
//--    Program counters.
int pc_1 = 1;
int pc_2 = 1;
//--  Thread thread1 global variables.
//--  Thread thread2 global variables.
//--  Shared Variables.
int z = 0;
_Bool x;
_Bool y;

//-- Another instrumentation variable: bNonDet used for non determinstic vars.
_Bool bNonDet;



void main()
{
  
  //-- Main Loop .. stops when no progress is made
  while ((progress==1)) {
    
    //-- Reset progress to none (false):
    progress = 0;
    
    
    //-- Context switch block:
    bNonDet = nondet_bool();
    if (!(pc_1 == 7) && (bNonDet==1)) {
      running = 1;
    }
    bNonDet = nondet_bool();
    if (!(pc_2 == 6) && (bNonDet==1)) {
      running = 2;
    }
    
    //-- The proper thread statements:
    
    //-- Thread number 1 statements:
    if (!(pc_1 == 7) && (running == 1)) {
      bNonDet = nondet_bool();
      if ((pc_1 == 1) && (bNonDet==1)) {
        x = 0;
        pc_1 = 2;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((x == 0) && ((pc_1 == 2) && (bNonDet==1))) {
        pc_1 = 3;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if (!(x == 0) && ((pc_1 == 2) && (bNonDet==1))) {
        pc_1 = 6;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_1 == 3) && (bNonDet==1)) {
        z = z + 1;
        pc_1 = 4;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_1 == 4) && (bNonDet==1)) {
        x = 1;
        pc_1 = 5;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_1 == 5) && (bNonDet==1)) {
        y = 0;
        pc_1 = 2;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_1 == 6) && (bNonDet==1)) {
        if (z >= 50) goto EXIT;
        pc_1 = 7;
        progress = 1;
      }
    }
    
    //-- Thread number 2 statements:
    if (!(pc_2 == 6) && (running == 2)) {
      bNonDet = nondet_bool();
      if ((pc_2 == 1) && (bNonDet==1)) {
        y = 0;
        pc_2 = 2;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((y == 0) && ((pc_2 == 2) && (bNonDet==1))) {
        pc_2 = 3;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if (!(y == 0) && ((pc_2 == 2) && (bNonDet==1))) {
        pc_2 = 6;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_2 == 3) && (bNonDet==1)) {
        z = z + 1;
        pc_2 = 4;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_2 == 4) && (bNonDet==1)) {
        y = 1;
        pc_2 = 5;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_2 == 5) && (bNonDet==1)) {
        x = 0;
        pc_2 = 2;
        progress = 1;
      }
    }
    
  }

  while(1) {}
  EXIT: return;
}
