/*-----Translation of program:Name-----*/

//-- Boolean non deterministic function definition.
_Bool nondet_bool(void);
int nondet_int(void);

//-- Assume function.
void assume(_Bool bVal) {
  if (!bVal) {
    while (1) {}
  } else {
    return;
  }
}

//-- Global Variables
//--  Instrumentation variables.
//--    Phase and phase prime variables, for each phase.
int phase_1 = 0;
int phase_2 = 0;
int phase_p_1 = 0;
int phase_p_2 = 0;
//--    Current "running" thread.
int running = 0;
//--    "progress": Force exploration progress.
_Bool progress = 1;
//--    "ret": Used to break out for a context switch from a (maybe nested) function call.
_Bool ret = 0;
//--    Program counters.
int pc_1 = 1;
int pc_2 = 1;
//--    Shared variables Instances: SV0, SV1, .. , SV(2*K+1).
int g_1_0 = 0;
int g_1_1 = 0;
int g_1_2 = 0;
int g_1_3 = 0;
int g_1_4 = 0;
int g_2_0 = 0;
int g_2_1 = 0;
int g_2_2 = 0;
int g_2_3 = 0;
int g_2_4 = 0;
int x_1_0 = 0;
int x_1_1 = 0;
int x_1_2 = 0;
int x_1_3 = 0;
int x_1_4 = 0;
int x_2_0 = 0;
int x_2_1 = 0;
int x_2_2 = 0;
int x_2_3 = 0;
int x_2_4 = 0;
int y_1_0 = 0;
int y_1_1 = 0;
int y_1_2 = 0;
int y_1_3 = 0;
int y_1_4 = 0;
int y_2_0 = 0;
int y_2_1 = 0;
int y_2_2 = 0;
int y_2_3 = 0;
int y_2_4 = 0;
_Bool m_1_0 = 1;
_Bool m_1_1 = 1;
_Bool m_1_2 = 1;
_Bool m_1_3 = 1;
_Bool m_1_4 = 1;
_Bool m_2_0 = 1;
_Bool m_2_1 = 1;
_Bool m_2_2 = 1;
_Bool m_2_3 = 1;
_Bool m_2_4 = 1;
//--  Thread thread1 global variables.
//--  Thread thread2 global variables.
//--  Shared Variables.
int g = 0;
int x = 0;
int y = 0;
_Bool m = 1;

//-- Another instrumentation variable: bNonDet used for non determinstic vars.
_Bool bNonDet;

void thread1_foo(int foo_r, int retpc)
{
  if (!(ret)) {
    if (foo_r == 0) {
      if (!(ret)) {
        x = 1;
      }
      //-- Begin CS-block
      bNonDet = nondet_bool();
      if (!(ret) && (bNonDet==1)) {
        if ((phase_p_1 == phase_1) && (phase_1 < 2)) {
          if (phase_1 == 0) {
            g_1_1 = g;
            x_1_1 = x;
            y_1_1 = y;
            m_1_1 = m;
          }
          if (phase_1 == 1) {
            g_1_2 = g;
            x_1_2 = x;
            y_1_2 = y;
            m_1_2 = m;
          }
          phase_1 = phase_1 + 1;
          ret = 1;
        }
        if ((phase_p_1 < phase_1) && !(ret)) {
          if ((phase_p_1 == 1) && ((x == x_1_3) && ((y == y_1_3) && ((m == m_1_3) && (g == g_1_3))))) {
            phase_p_1 = 2;
            g = g_1_4;
            x = x_1_4;
            y = y_1_4;
            m = m_1_4;
          }
          if ((phase_p_1 == 0) && ((x == x_1_1) && ((y == y_1_1) && ((m == m_1_1) && (g == g_1_1))))) {
            phase_p_1 = 1;
            g = g_1_2;
            x = x_1_2;
            y = y_1_2;
            m = m_1_2;
          }
        }
      }
      //-- End CS-block
      if (!(ret)) {
        y = 1;
      }
      //-- Begin CS-block
      bNonDet = nondet_bool();
      if (!(ret) && (bNonDet==1)) {
        if ((phase_p_1 == phase_1) && (phase_1 < 2)) {
          if (phase_1 == 0) {
            g_1_1 = g;
            x_1_1 = x;
            y_1_1 = y;
            m_1_1 = m;
          }
          if (phase_1 == 1) {
            g_1_2 = g;
            x_1_2 = x;
            y_1_2 = y;
            m_1_2 = m;
          }
          phase_1 = phase_1 + 1;
          ret = 1;
        }
        if ((phase_p_1 < phase_1) && !(ret)) {
          if ((phase_p_1 == 1) && ((x == x_1_3) && ((y == y_1_3) && ((m == m_1_3) && (g == g_1_3))))) {
            phase_p_1 = 2;
            g = g_1_4;
            x = x_1_4;
            y = y_1_4;
            m = m_1_4;
          }
          if ((phase_p_1 == 0) && ((x == x_1_1) && ((y == y_1_1) && ((m == m_1_1) && (g == g_1_1))))) {
            phase_p_1 = 1;
            g = g_1_2;
            x = x_1_2;
            y = y_1_2;
            m = m_1_2;
          }
        }
      }
      //-- End CS-block
      if (!(ret)) {
        thread1_foo(foo_r, 0);
      }
    } else {
      if (!(ret)) {
        thread1_acquire(0);
      }
      //-- Begin CS-block
      bNonDet = nondet_bool();
      if (!(ret) && (bNonDet==1)) {
        if ((phase_p_1 == phase_1) && (phase_1 < 2)) {
          if (phase_1 == 0) {
            g_1_1 = g;
            x_1_1 = x;
            y_1_1 = y;
            m_1_1 = m;
          }
          if (phase_1 == 1) {
            g_1_2 = g;
            x_1_2 = x;
            y_1_2 = y;
            m_1_2 = m;
          }
          phase_1 = phase_1 + 1;
          ret = 1;
        }
        if ((phase_p_1 < phase_1) && !(ret)) {
          if ((phase_p_1 == 1) && ((x == x_1_3) && ((y == y_1_3) && ((m == m_1_3) && (g == g_1_3))))) {
            phase_p_1 = 2;
            g = g_1_4;
            x = x_1_4;
            y = y_1_4;
            m = m_1_4;
          }
          if ((phase_p_1 == 0) && ((x == x_1_1) && ((y == y_1_1) && ((m == m_1_1) && (g == g_1_1))))) {
            phase_p_1 = 1;
            g = g_1_2;
            x = x_1_2;
            y = y_1_2;
            m = m_1_2;
          }
        }
      }
      //-- End CS-block
      if (!(ret)) {
        g = 0;
      }
      //-- Begin CS-block
      bNonDet = nondet_bool();
      if (!(ret) && (bNonDet==1)) {
        if ((phase_p_1 == phase_1) && (phase_1 < 2)) {
          if (phase_1 == 0) {
            g_1_1 = g;
            x_1_1 = x;
            y_1_1 = y;
            m_1_1 = m;
          }
          if (phase_1 == 1) {
            g_1_2 = g;
            x_1_2 = x;
            y_1_2 = y;
            m_1_2 = m;
          }
          phase_1 = phase_1 + 1;
          ret = 1;
        }
        if ((phase_p_1 < phase_1) && !(ret)) {
          if ((phase_p_1 == 1) && ((x == x_1_3) && ((y == y_1_3) && ((m == m_1_3) && (g == g_1_3))))) {
            phase_p_1 = 2;
            g = g_1_4;
            x = x_1_4;
            y = y_1_4;
            m = m_1_4;
          }
          if ((phase_p_1 == 0) && ((x == x_1_1) && ((y == y_1_1) && ((m == m_1_1) && (g == g_1_1))))) {
            phase_p_1 = 1;
            g = g_1_2;
            x = x_1_2;
            y = y_1_2;
            m = m_1_2;
          }
        }
      }
      //-- End CS-block
      if (!(ret)) {
        g = 1;
      }
      //-- Begin CS-block
      bNonDet = nondet_bool();
      if (!(ret) && (bNonDet==1)) {
        if ((phase_p_1 == phase_1) && (phase_1 < 2)) {
          if (phase_1 == 0) {
            g_1_1 = g;
            x_1_1 = x;
            y_1_1 = y;
            m_1_1 = m;
          }
          if (phase_1 == 1) {
            g_1_2 = g;
            x_1_2 = x;
            y_1_2 = y;
            m_1_2 = m;
          }
          phase_1 = phase_1 + 1;
          ret = 1;
        }
        if ((phase_p_1 < phase_1) && !(ret)) {
          if ((phase_p_1 == 1) && ((x == x_1_3) && ((y == y_1_3) && ((m == m_1_3) && (g == g_1_3))))) {
            phase_p_1 = 2;
            g = g_1_4;
            x = x_1_4;
            y = y_1_4;
            m = m_1_4;
          }
          if ((phase_p_1 == 0) && ((x == x_1_1) && ((y == y_1_1) && ((m == m_1_1) && (g == g_1_1))))) {
            phase_p_1 = 1;
            g = g_1_2;
            x = x_1_2;
            y = y_1_2;
            m = m_1_2;
          }
        }
      }
      //-- End CS-block
      if (!(ret)) {
        thread1_release(0);
      }
    }
  }
  
  //-- Update the program counter value.
  if (!(ret) && (retpc > 0)) {
    if (phase_p_1 == phase_1) {
      pc_1 = retpc;
    } else {
      assume(0);
    }
  }
}

void thread1_acquire(int retpc)
{
  _Bool thread1_acquire_temp = 0;
  if (!(ret)) {
    while (!(thread1_acquire_temp)) {
      if (!(ret)) {
        /* Atomic stmt starts.*/
          if ((m==1)) {
            m = 0;
            thread1_acquire_temp = 1;
          }
        /* Atomic stmt ends.*/

      }
    }
  }
  
  //-- Update the program counter value.
  if (!(ret) && (retpc > 0)) {
    if (phase_p_1 == phase_1) {
      pc_1 = retpc;
    } else {
      assume(0);
    }
  }
}

void thread1_release(int retpc)
{
  if (!(ret)) {
    m = 1;
  }
  
  //-- Update the program counter value.
  if (!(ret) && (retpc > 0)) {
    if (phase_p_1 == phase_1) {
      pc_1 = retpc;
    } else {
      assume(0);
    }
  }
}

void thread2_foo(int foo_r, int retpc)
{
  if (!(ret)) {
    if (foo_r == 0) {
      if (!(ret)) {
        x = 1;
      }
      //-- Begin CS-block
      bNonDet = nondet_bool();
      if (!(ret) && (bNonDet==1)) {
        if ((phase_p_2 == phase_2) && (phase_2 < 2)) {
          if (phase_2 == 0) {
            g_2_1 = g;
            x_2_1 = x;
            y_2_1 = y;
            m_2_1 = m;
          }
          if (phase_2 == 1) {
            g_2_2 = g;
            x_2_2 = x;
            y_2_2 = y;
            m_2_2 = m;
          }
          phase_2 = phase_2 + 1;
          ret = 1;
        }
        if ((phase_p_2 < phase_2) && !(ret)) {
          if ((phase_p_2 == 1) && ((x == x_2_3) && ((y == y_2_3) && ((m == m_2_3) && (g == g_2_3))))) {
            phase_p_2 = 2;
            g = g_2_4;
            x = x_2_4;
            y = y_2_4;
            m = m_2_4;
          }
          if ((phase_p_2 == 0) && ((x == x_2_1) && ((y == y_2_1) && ((m == m_2_1) && (g == g_2_1))))) {
            phase_p_2 = 1;
            g = g_2_2;
            x = x_2_2;
            y = y_2_2;
            m = m_2_2;
          }
        }
      }
      //-- End CS-block
      if (!(ret)) {
        y = 1;
      }
      //-- Begin CS-block
      bNonDet = nondet_bool();
      if (!(ret) && (bNonDet==1)) {
        if ((phase_p_2 == phase_2) && (phase_2 < 2)) {
          if (phase_2 == 0) {
            g_2_1 = g;
            x_2_1 = x;
            y_2_1 = y;
            m_2_1 = m;
          }
          if (phase_2 == 1) {
            g_2_2 = g;
            x_2_2 = x;
            y_2_2 = y;
            m_2_2 = m;
          }
          phase_2 = phase_2 + 1;
          ret = 1;
        }
        if ((phase_p_2 < phase_2) && !(ret)) {
          if ((phase_p_2 == 1) && ((x == x_2_3) && ((y == y_2_3) && ((m == m_2_3) && (g == g_2_3))))) {
            phase_p_2 = 2;
            g = g_2_4;
            x = x_2_4;
            y = y_2_4;
            m = m_2_4;
          }
          if ((phase_p_2 == 0) && ((x == x_2_1) && ((y == y_2_1) && ((m == m_2_1) && (g == g_2_1))))) {
            phase_p_2 = 1;
            g = g_2_2;
            x = x_2_2;
            y = y_2_2;
            m = m_2_2;
          }
        }
      }
      //-- End CS-block
      if (!(ret)) {
        thread2_foo(foo_r, 0);
      }
    } else {
      if (!(ret)) {
        thread2_acquire(0);
      }
      //-- Begin CS-block
      bNonDet = nondet_bool();
      if (!(ret) && (bNonDet==1)) {
        if ((phase_p_2 == phase_2) && (phase_2 < 2)) {
          if (phase_2 == 0) {
            g_2_1 = g;
            x_2_1 = x;
            y_2_1 = y;
            m_2_1 = m;
          }
          if (phase_2 == 1) {
            g_2_2 = g;
            x_2_2 = x;
            y_2_2 = y;
            m_2_2 = m;
          }
          phase_2 = phase_2 + 1;
          ret = 1;
        }
        if ((phase_p_2 < phase_2) && !(ret)) {
          if ((phase_p_2 == 1) && ((x == x_2_3) && ((y == y_2_3) && ((m == m_2_3) && (g == g_2_3))))) {
            phase_p_2 = 2;
            g = g_2_4;
            x = x_2_4;
            y = y_2_4;
            m = m_2_4;
          }
          if ((phase_p_2 == 0) && ((x == x_2_1) && ((y == y_2_1) && ((m == m_2_1) && (g == g_2_1))))) {
            phase_p_2 = 1;
            g = g_2_2;
            x = x_2_2;
            y = y_2_2;
            m = m_2_2;
          }
        }
      }
      //-- End CS-block
      if (!(ret)) {
        g = 0;
      }
      //-- Begin CS-block
      bNonDet = nondet_bool();
      if (!(ret) && (bNonDet==1)) {
        if ((phase_p_2 == phase_2) && (phase_2 < 2)) {
          if (phase_2 == 0) {
            g_2_1 = g;
            x_2_1 = x;
            y_2_1 = y;
            m_2_1 = m;
          }
          if (phase_2 == 1) {
            g_2_2 = g;
            x_2_2 = x;
            y_2_2 = y;
            m_2_2 = m;
          }
          phase_2 = phase_2 + 1;
          ret = 1;
        }
        if ((phase_p_2 < phase_2) && !(ret)) {
          if ((phase_p_2 == 1) && ((x == x_2_3) && ((y == y_2_3) && ((m == m_2_3) && (g == g_2_3))))) {
            phase_p_2 = 2;
            g = g_2_4;
            x = x_2_4;
            y = y_2_4;
            m = m_2_4;
          }
          if ((phase_p_2 == 0) && ((x == x_2_1) && ((y == y_2_1) && ((m == m_2_1) && (g == g_2_1))))) {
            phase_p_2 = 1;
            g = g_2_2;
            x = x_2_2;
            y = y_2_2;
            m = m_2_2;
          }
        }
      }
      //-- End CS-block
      if (!(ret)) {
        g = 1;
      }
      //-- Begin CS-block
      bNonDet = nondet_bool();
      if (!(ret) && (bNonDet==1)) {
        if ((phase_p_2 == phase_2) && (phase_2 < 2)) {
          if (phase_2 == 0) {
            g_2_1 = g;
            x_2_1 = x;
            y_2_1 = y;
            m_2_1 = m;
          }
          if (phase_2 == 1) {
            g_2_2 = g;
            x_2_2 = x;
            y_2_2 = y;
            m_2_2 = m;
          }
          phase_2 = phase_2 + 1;
          ret = 1;
        }
        if ((phase_p_2 < phase_2) && !(ret)) {
          if ((phase_p_2 == 1) && ((x == x_2_3) && ((y == y_2_3) && ((m == m_2_3) && (g == g_2_3))))) {
            phase_p_2 = 2;
            g = g_2_4;
            x = x_2_4;
            y = y_2_4;
            m = m_2_4;
          }
          if ((phase_p_2 == 0) && ((x == x_2_1) && ((y == y_2_1) && ((m == m_2_1) && (g == g_2_1))))) {
            phase_p_2 = 1;
            g = g_2_2;
            x = x_2_2;
            y = y_2_2;
            m = m_2_2;
          }
        }
      }
      //-- End CS-block
      if (!(ret)) {
        thread2_release(0);
      }
    }
  }
  
  //-- Update the program counter value.
  if (!(ret) && (retpc > 0)) {
    if (phase_p_2 == phase_2) {
      pc_2 = retpc;
    } else {
      assume(0);
    }
  }
}

void thread2_acquire(int retpc)
{
  _Bool thread2_acquire_temp = 0;
  if (!(ret)) {
    while (!(thread2_acquire_temp)) {
      if (!(ret)) {
        /* Atomic stmt starts.*/
          if ((m==1)) {
            m = 0;
            thread2_acquire_temp = 1;
          }
        /* Atomic stmt ends.*/

      }
    }
  }
  
  //-- Update the program counter value.
  if (!(ret) && (retpc > 0)) {
    if (phase_p_2 == phase_2) {
      pc_2 = retpc;
    } else {
      assume(0);
    }
  }
}

void thread2_release(int retpc)
{
  if (!(ret)) {
    m = 1;
  }
  
  //-- Update the program counter value.
  if (!(ret) && (retpc > 0)) {
    if (phase_p_2 == phase_2) {
      pc_2 = retpc;
    } else {
      assume(0);
    }
  }
}

void main()
{
  int thread1_main_q;
  _Bool thread1_main_b;
  int thread2_main_q;
  _Bool thread2_main_b;
  
  //-- Main Loop .. stops when no progress is made
  while ((progress==1)) {
    
    //-- Reset progress to none (false):
    progress = 0;
    
    
    //-- Context switch block:
    bNonDet = nondet_bool();
    if (!(pc_1 == 9) && (bNonDet==1)) {
      running = 1;
    }
    bNonDet = nondet_bool();
    if (!(pc_2 == 9) && (bNonDet==1)) {
      running = 2;
    }
    
    //-- The proper thread statements:
    
    //-- Thread number 1 statements:
    if (!(pc_1 == 9) && (running == 1)) {
      bNonDet = nondet_bool();
      if ((pc_1 == 1) && (bNonDet==1)) {
        bNonDet = nondet_bool();
        thread1_main_b = (bNonDet==1);
        pc_1 = 2;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((thread1_main_b==1) && ((pc_1 == 2) && (bNonDet==1))) {
        pc_1 = 3;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if (!(thread1_main_b) && ((pc_1 == 2) && (bNonDet==1))) {
        pc_1 = 4;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_1 == 4) && (bNonDet==1)) {
        thread1_main_q = 1;
        pc_1 = 5;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_1 == 3) && (bNonDet==1)) {
        thread1_main_q = 0;
        pc_1 = 5;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_1 == 5) && (bNonDet==1)) {
        //-- Function call: save copy of shared variables
        if (phase_1 == 0) {
          g_1_0 = g;
          x_1_0 = x;
          y_1_0 = y;
          m_1_0 = m;
          g = g_1_0;
          x = x_1_0;
          y = y_1_0;
          m = m_1_0;
        }
        if (phase_1 == 1) {
          g_1_2 = g;
          x_1_2 = x;
          y_1_2 = y;
          m_1_2 = m;
          g = g_1_0;
          x = x_1_0;
          y = y_1_0;
          m = m_1_0;
        }
        if (phase_1 == 2) {
          g_1_4 = g;
          x_1_4 = x;
          y_1_4 = y;
          m_1_4 = m;
          g = g_1_0;
          x = x_1_0;
          y = y_1_0;
          m = m_1_0;
        }
        thread1_foo(thread1_main_q, 6);
        progress = 1;
        //-- Post Function Call:
        if ((phase_1 <= 2) && !(ret)) {
          phase_p_1 = 0;
          phase_1 = 0;
        }
        if ((phase_1 <= 2) && (ret==1)) {
          ret = 0;
          phase_p_1 = 0;
        }
      }
      bNonDet = nondet_bool();
      if ((pc_1 == 6) && (bNonDet==1)) {
        //-- Function call: save copy of shared variables
        if (phase_1 == 0) {
          g_1_0 = g;
          x_1_0 = x;
          y_1_0 = y;
          m_1_0 = m;
          g = g_1_0;
          x = x_1_0;
          y = y_1_0;
          m = m_1_0;
        }
        if (phase_1 == 1) {
          g_1_2 = g;
          x_1_2 = x;
          y_1_2 = y;
          m_1_2 = m;
          g = g_1_0;
          x = x_1_0;
          y = y_1_0;
          m = m_1_0;
        }
        if (phase_1 == 2) {
          g_1_4 = g;
          x_1_4 = x;
          y_1_4 = y;
          m_1_4 = m;
          g = g_1_0;
          x = x_1_0;
          y = y_1_0;
          m = m_1_0;
        }
        thread1_acquire(7);
        progress = 1;
        //-- Post Function Call:
        if ((phase_1 <= 2) && !(ret)) {
          phase_p_1 = 0;
          phase_1 = 0;
        }
        if ((phase_1 <= 2) && (ret==1)) {
          ret = 0;
          phase_p_1 = 0;
        }
      }
      bNonDet = nondet_bool();
      if ((pc_1 == 7) && (bNonDet==1)) {
        if (g < 1) goto EXIT;
        pc_1 = 8;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_1 == 8) && (bNonDet==1)) {
        //-- Function call: save copy of shared variables
        if (phase_1 == 0) {
          g_1_0 = g;
          x_1_0 = x;
          y_1_0 = y;
          m_1_0 = m;
          g = g_1_0;
          x = x_1_0;
          y = y_1_0;
          m = m_1_0;
        }
        if (phase_1 == 1) {
          g_1_2 = g;
          x_1_2 = x;
          y_1_2 = y;
          m_1_2 = m;
          g = g_1_0;
          x = x_1_0;
          y = y_1_0;
          m = m_1_0;
        }
        if (phase_1 == 2) {
          g_1_4 = g;
          x_1_4 = x;
          y_1_4 = y;
          m_1_4 = m;
          g = g_1_0;
          x = x_1_0;
          y = y_1_0;
          m = m_1_0;
        }
        thread1_release(9);
        progress = 1;
        //-- Post Function Call:
        if ((phase_1 <= 2) && !(ret)) {
          phase_p_1 = 0;
          phase_1 = 0;
        }
        if ((phase_1 <= 2) && (ret==1)) {
          ret = 0;
          phase_p_1 = 0;
        }
      }
    }
    
    //-- Thread number 2 statements:
    if (!(pc_2 == 9) && (running == 2)) {
      bNonDet = nondet_bool();
      if ((pc_2 == 1) && (bNonDet==1)) {
        bNonDet = nondet_bool();
        thread2_main_b = (bNonDet==1);
        pc_2 = 2;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((thread2_main_b==1) && ((pc_2 == 2) && (bNonDet==1))) {
        pc_2 = 3;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if (!(thread2_main_b) && ((pc_2 == 2) && (bNonDet==1))) {
        pc_2 = 4;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_2 == 4) && (bNonDet==1)) {
        thread2_main_q = 1;
        pc_2 = 5;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_2 == 3) && (bNonDet==1)) {
        thread2_main_q = 0;
        pc_2 = 5;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_2 == 5) && (bNonDet==1)) {
        //-- Function call: save copy of shared variables
        if (phase_2 == 0) {
          g_2_0 = g;
          x_2_0 = x;
          y_2_0 = y;
          m_2_0 = m;
          g = g_2_0;
          x = x_2_0;
          y = y_2_0;
          m = m_2_0;
        }
        if (phase_2 == 1) {
          g_2_2 = g;
          x_2_2 = x;
          y_2_2 = y;
          m_2_2 = m;
          g = g_2_0;
          x = x_2_0;
          y = y_2_0;
          m = m_2_0;
        }
        if (phase_2 == 2) {
          g_2_4 = g;
          x_2_4 = x;
          y_2_4 = y;
          m_2_4 = m;
          g = g_2_0;
          x = x_2_0;
          y = y_2_0;
          m = m_2_0;
        }
        thread2_foo(thread2_main_q, 6);
        progress = 1;
        //-- Post Function Call:
        if ((phase_2 <= 2) && !(ret)) {
          phase_p_2 = 0;
          phase_2 = 0;
        }
        if ((phase_2 <= 2) && (ret==1)) {
          ret = 0;
          phase_p_2 = 0;
        }
      }
      bNonDet = nondet_bool();
      if ((pc_2 == 6) && (bNonDet==1)) {
        //-- Function call: save copy of shared variables
        if (phase_2 == 0) {
          g_2_0 = g;
          x_2_0 = x;
          y_2_0 = y;
          m_2_0 = m;
          g = g_2_0;
          x = x_2_0;
          y = y_2_0;
          m = m_2_0;
        }
        if (phase_2 == 1) {
          g_2_2 = g;
          x_2_2 = x;
          y_2_2 = y;
          m_2_2 = m;
          g = g_2_0;
          x = x_2_0;
          y = y_2_0;
          m = m_2_0;
        }
        if (phase_2 == 2) {
          g_2_4 = g;
          x_2_4 = x;
          y_2_4 = y;
          m_2_4 = m;
          g = g_2_0;
          x = x_2_0;
          y = y_2_0;
          m = m_2_0;
        }
        thread2_acquire(7);
        progress = 1;
        //-- Post Function Call:
        if ((phase_2 <= 2) && !(ret)) {
          phase_p_2 = 0;
          phase_2 = 0;
        }
        if ((phase_2 <= 2) && (ret==1)) {
          ret = 0;
          phase_p_2 = 0;
        }
      }
      bNonDet = nondet_bool();
      if ((pc_2 == 7) && (bNonDet==1)) {
        if (g < 1) goto EXIT;
        pc_2 = 8;
        progress = 1;
      }
      bNonDet = nondet_bool();
      if ((pc_2 == 8) && (bNonDet==1)) {
        //-- Function call: save copy of shared variables
        if (phase_2 == 0) {
          g_2_0 = g;
          x_2_0 = x;
          y_2_0 = y;
          m_2_0 = m;
          g = g_2_0;
          x = x_2_0;
          y = y_2_0;
          m = m_2_0;
        }
        if (phase_2 == 1) {
          g_2_2 = g;
          x_2_2 = x;
          y_2_2 = y;
          m_2_2 = m;
          g = g_2_0;
          x = x_2_0;
          y = y_2_0;
          m = m_2_0;
        }
        if (phase_2 == 2) {
          g_2_4 = g;
          x_2_4 = x;
          y_2_4 = y;
          m_2_4 = m;
          g = g_2_0;
          x = x_2_0;
          y = y_2_0;
          m = m_2_0;
        }
        thread2_release(9);
        progress = 1;
        //-- Post Function Call:
        if ((phase_2 <= 2) && !(ret)) {
          phase_p_2 = 0;
          phase_2 = 0;
        }
        if ((phase_2 <= 2) && (ret==1)) {
          ret = 0;
          phase_p_2 = 0;
        }
      }
    }
    
  }
  while (1) {}
EXIT: return;
}
